/*
 * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function(b){b.support.touch="ontouchend" in document;if(!b.support.touch){return;}var c=b.ui.mouse.prototype,e=c._mouseInit,a;function d(g,h){if(g.originalEvent.touches.length>1){return;}g.preventDefault();var i=g.originalEvent.changedTouches[0],f=document.createEvent("MouseEvents");f.initMouseEvent(h,true,true,window,1,i.screenX,i.screenY,i.clientX,i.clientY,false,false,false,false,0,null);g.target.dispatchEvent(f);}c._touchStart=function(g){var f=this;if(a||!f._mouseCapture(g.originalEvent.changedTouches[0])){return;}a=true;f._touchMoved=false;d(g,"mouseover");d(g,"mousemove");d(g,"mousedown");};c._touchMove=function(f){if(!a){return;}this._touchMoved=true;d(f,"mousemove");};c._touchEnd=function(f){if(!a){return;}d(f,"mouseup");d(f,"mouseout");if(!this._touchMoved){d(f,"click");}a=false;};c._mouseInit=function(){var f=this;f.element.bind("touchstart",b.proxy(f,"_touchStart")).bind("touchmove",b.proxy(f,"_touchMove")).bind("touchend",b.proxy(f,"_touchEnd"));e.call(f);};})(jQuery);

$(document).ready(function() {

// Preload field images
$('<images />').attr('src','images/field-horizontal.png');
$('<images />').attr('src','images/field-vertical.png');

// Global variables
var body = $('body'),
    field = $('#field'),
    hfield = "<images id='field-horizontal' src='images/field-horizontal.png' />",
    vfield = "<images id='field-vertical' src='images/field-vertical.png' />";

// Load field based on window size
$(window).load(function() {
var h = $(window).height(),
    w = $(window).width(),
    vc = Math.round(h/2),
    hc = Math.round(w/2);
 if (w > h) {
   body.append(hfield);
   if (window.localStorage && localStorage.getItem("soccer_hor_dgm") != null) {
    field.html(localStorage.getItem("soccer_hor_dgm"));
   } else {
    hor_442(vc,hc);
   }
 } else {
   body.append(vfield);
   if (window.localStorage && localStorage.getItem("soccer_vert_dgm") != null) {
    field.html(localStorage.getItem("soccer_vert_dgm"));
   } else {
    vert_442(vc,hc);
   }
 }
 $('.drag').draggable({cursor: 'move'});
});

// Adjust field based on window size
$(window).resize(function() {
var h = $(window).height(),
    w = $(window).width(),
    vc = Math.round(h/2),
    hc = Math.round(w/2),
    hf = $('#field-horizontal'),
    vf = $('#field-vertical');
 if (w > h) {
  if (vf.length) { 
   vf.remove(); 
  }
  if (!hf.length) {
   body.append(hfield);
  }
 } 
 if (h > w) {
  if (hf.length) {
   hf.remove();
  }
  if (!vf.length) {
   body.append(vfield);
  }
 }
 $('.drag').draggable({cursor: 'move'});
});

// Formations pop up.
$('#formation').on('click',function(e) {
 e.preventDefault();
 $('#formations').dialog({position:"absolute",closeText:"Close",draggable:false,resizable:false});
});

// Load formation.
$('.formation').on('click',function(e) {
e.preventDefault();
var h = $(window).height(),
    w = $(window).width(),
    vc = Math.round(h/2),
    hc = Math.round(w/2),
    land = "",
    id = $(this).attr('id');
 if (w > h) { land = "hor"; } else { land = "vert"; }
 window[land+"_"+id](vc,hc);
 $('#formations').dialog('close');
});

// If browser can handle local storage, save player positions on field.
$('#save').on('click',function(e) {
 e.preventDefault();
 if (window.localStorage) {
  var land = "",
      h = $(window).height(),
      w = $(window).width(),
      players = field.html();
  if (w > h) { land = "hor"; } else { land = "vert"; } 
  if (players.length) {
   localStorage.setItem('soccer_'+land+'_dgm',players);
   $('#success').dialog({position:"absolute",closeText:"Close",draggable:false,resizable:false});
  }
 } else {
  $('#error').dialog({position:"absolute",closeText:"Close",draggable:false,resizable:false});
 }
});

});

// 4-4-2 horizontal formation.
function hor_442(vc,hc) {
 var home="",tp=20,lft=50,i=0,htopoffset=vc-100,hleftoffset=hc-200,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  tp+=52;
  i++;
  if (i==4) {tp=20,lft=180;}
  if (i==8) {tp=72,lft=350;}
  if (i==9) {tp=124, lft=350;}
 }
 
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 
 $('#field').html(home+ball);
 $('.drag').draggable({cursor: 'move'});
}

// 4-3-3 horizontal formation.
function hor_433(vc,hc) {
 var home="",tp=20,lft=50,i=0,htopoffset=vc-100,hleftoffset=hc-200,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  tp+=52;
  i++;
  if (i==4) {tp=52,lft=180;}
  if (i==7) {tp=52,lft=350;}
 }
 
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 
 $('#field').html(home+ball);
 $('.drag').draggable({cursor: 'move'});
}

// 3-5-2 horizontal formation.
function hor_352(vc,hc) {
 var home="",tp=42,lft=50,i=0,htopoffset=vc-100,hleftoffset=hc-200,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  tp+=52;
  i++;
  if (i==3) {tp=-10,lft=180;}
  if (i==8) {tp=68,lft=350;}
  if (i==9) {tp=120, lft=350;}
 }
 
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 
 $('#field').html(home+ball);
 $('.drag').draggable({cursor: 'move'});
}

// 4-4-2 vertical formation.
function vert_442(vc,hc) {
 var home="",tp=0,lft=0,i=0,htopoffset=hc-100,hleftoffset=hc-100,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==4) {tp=52,lft=0;}
  if (i==8) {tp=104,lft=52;}
  if (i==9) {lft=104;}
 }
 var away="",tp=104,lft=0,i=0,atopoffset=hc+100,aleftoffset=hc-100;
 for (p=0; p<10; p++) {
  away += "<images class='drag dropped' src='images/jersey-away.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==4) {tp=52,lft=0;}
  if (i==8) {tp=0,lft=52;}
  if (i==9) {lft=104;}
 }
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 away = "<div id='away-formation' style='position:absolute; top:"+atopoffset+"px; left:"+aleftoffset+"px;'>"+away+"</div>";
 $('#field').html(home+away+ball);
 $('.drag').draggable({cursor: 'move'});
}

// 4-3-3 vertical formation.
function vert_433(vc,hc) {
 var home="",tp=0,lft=0,i=0,htopoffset=hc-100,hleftoffset=hc-100,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==4) {tp=52,lft=32;}
  if (i==7) {tp=104,lft=32;}
 }
 var away="",tp=104,lft=0,i=0,atopoffset=hc+100,aleftoffset=hc-100;
 for (p=0; p<10; p++) {
  away += "<images class='drag dropped' src='images/jersey-away.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==4) {tp=52,lft=32;}
  if (i==7) {tp=0,lft=32;}
 }
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 away = "<div id='away-formation' style='position:absolute; top:"+atopoffset+"px; left:"+aleftoffset+"px;'>"+away+"</div>";
 $('#field').html(home+away+ball);
 $('.drag').draggable({cursor: 'move'});
}

// 3-5-2 vertical formation.
function vert_352(vc,hc) {
 var home="",tp=0,lft=52,i=0,htopoffset=hc-100,hleftoffset=hc-100,
     ball = "<images class='drag dropped' src='images/ball.png' width='32' height='32' style='position:absolute; top:"+vc+"px; left:"+hc+"px' />";
 for (p=0; p<10; p++) {
  home += "<images class='drag dropped' src='images/jersey-home.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==3) {tp=52,lft=0;}
  if (i==8) {tp=104,lft=68;}
  if (i==9) {lft=120;}
 }
 var away="",tp=104,lft=52,i=0,atopoffset=hc+100,aleftoffset=hc-100;
 for (p=0; p<10; p++) {
  away += "<images class='drag dropped' src='images/jersey-away.png' width='32' height='32' style='position:absolute; top:"+tp+"px; left:"+lft+"px;' />";
  lft+=52;
  i++;
  if (i==3) {tp=52,lft=0;}
  if (i==8) {tp=0,lft=68;}
  if (i==9) {lft=120;}
 }
 home = "<div id='home-formation' style='position:absolute; top:"+htopoffset+"px; left:"+hleftoffset+"px;'>"+home+"</div>";
 away = "<div id='away-formation' style='position:absolute; top:"+atopoffset+"px; left:"+aleftoffset+"px;'>"+away+"</div>";
 $('#field').html(home+away+ball);
 $('.drag').draggable({cursor: 'move'});
}

