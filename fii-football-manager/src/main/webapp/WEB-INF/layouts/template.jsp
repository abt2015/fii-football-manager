<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>

<!DOCTYPE html>
<html>
<head>
<style>
.footer {
	position: relative;
	margin-top: -150px; /* negative value of footer height */
	height: 150px;
	clear: both;
	padding-top: 20px;
}

.container-fluid {
	margin: 0 auto;
	min-height: 100%;
	padding: 20px 0;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}

navbar-brand {
	width: 25px;
	height: 25px;
	background-color: #F2F8FB;
	background-size: 25px;
}

.nav-tabs {
	display: inline-block;
	border-bottom: none;
	padding-top: 0px;
	font-weight: bold;
}

.nav-list>li {
	padding: 10px 5px 5px;
	border-left: 1px solid #eee;
	line-width: 250px;
	width: 250px;
}

.nav-list>li:first-child {
	border-left: none;
}

.nav-list>li>a>b:hover {
	text-decoration: none;
}

.nav-list>li>a>span {
	display: block;
	font-weight: bold;
	text-transform: uppercase;
}

.mega-dropdown {
	position: static !important;
}

.mega-dropdown-menu {
	padding: 25px 15px 15px;
	text-align: center;
	width: 100%;
}
}
</style>
<link
	href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css"
	rel="stylesheet">
<script
	src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /></title>
</head>
<tilesx:useAttribute name="current" />
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<spring:url value="/"/>">FII
					Football Manager</a>

			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="current == 'home' ? 'active' : ''"><a
						href="<spring:url value="/"/>">Home</a></li>
					<!-- Status bar -->
					<security:authorize access="isAuthenticated()">
						<li class="dropdown mega-dropdown"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown">Status <span
								class="${notif == '0' ? '' : 'glyphicon glyphicon-circle-arrow-down'}"
								style="color: red"> </span>
						</a>
							<div class="dropdown-menu mega-dropdown-menu">
								<div class="container-full">
									<div class="tab-content">
										<div class="tab-pane active" id="status">
											<ul class="nav-list list-inline">
												<li><a><b>Buget</b></a>: <c:out value="${gbuget}" /></li>
												<li><a
													href="<spring:url value="/transfers/offers.html"/>"> <b>Transfer
															offers:</b> <c:out value="${offersNo}" />
												</a></li>
												<li><a
													href="<spring:url value="/team/challenges.html"/>"> <b>Challenges:
															${gmatches }</b>
												</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div></li>
					</security:authorize>
					<!-- /Status bar -->
					<security:authorize access="!isAuthenticated()">
						<li class="${current == 'register' ? 'active' : ''}"><a
							href="<spring:url value="/register.html"/>">Register</a></li>
					</security:authorize>

					<security:authorize access="hasRole('ADMIN')">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">Admin
								<span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li class="dropdown-header">Players</li>
								<li><a
									href="<spring:url value="/admin-side/add-player.html"/>">Add
										player</a></li>
								<li class="dropdown-header">Teams</li>
								<li><a
									href="<spring:url value="/admin-side/add-team.html"/>">Add
										team</a>
								<li><a
									href="<spring:url value="/admin-side/update-teams.html"/>">
										Update teams </a>
							</ul>
					</security:authorize>
					<security:authorize access="isAuthenticated()">
						<li><a href="<spring:url value="/manager/my-profile.html"/>">My
								Profile</a></li>
					</security:authorize>
					<security:authorize access="isAuthenticated()">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false"> <c:choose>
									<c:when test="${gteam eq 'null'}">
										No team
									</c:when>
									<c:otherwise>
										${gteam}
									</c:otherwise>
								</c:choose> <span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li class="divider">Matches</li>
								<li><a href="<spring:url value="/team/challenges.html"/>">Challenges</a>
								<li><a href="<spring:url value="/team/results.html"/>">Results</a>
								<li class="divider"></li>
								<li><a href="<spring:url value="/team/formation.html"/>">Formation</a></li>
								<li><a href="<spring:url value="/team/squad.html"/>">Squad</a></li>
								<li class="dropdown-header">Transfers</li>
								<li class="divider"></li>
								<li><a href="<spring:url value="/transfers/offers.html"/>">Transfer
										Offers</a></li>
								<li><a href="<spring:url value="/players/1.html"/>">Scouting</a></li>
								<li class="divider"></li>
								<li class="dropdown-header">Matches</li>
								<li><a href="#">Schedule</a></li>
							</ul></li>
					</security:authorize>

					<security:authorize access="isAuthenticated()">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">World
								<span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li class="dropdown-header">Leagues</li>
								<li><a href="<spring:url value="/leagues.html"/>">National
										Leagues</a></li>
								<li><a href="#">International Leagues</a></li>
								<li class="divider"></li>
								<li class="dropdown-header">Jobs</li>
								<li><a href="<spring:url value="/free-teams.html"/>">Free
										Teams</a></li>
							</ul></li>
					</security:authorize>
				</ul>

				<ul class="nav navbar-nav navbar-right">

					<security:authorize access="!isAuthenticated()">
						<form class="navbar-form navbar-right" role="form"
							action="<spring:url value="/j_spring_security_check" />"
							method="POST">
							<input type="text" name="j_username" class="form-control"
								placeholder="Username" required autofocus> <input
								type="password" name="j_password" class="form-control"
								placeholder="Password" required>
							<button type="submit" class="btn btn-primary">Sign in</button>
						</form>
					</security:authorize>

					<security:authorize access="isAuthenticated()">
						<li><a href="<spring:url value="/logout"/>">Logout</a></li>
					</security:authorize>
				</ul>
			</div>
		</div>
		<tiles:insertAttribute name="status-bar" />
	</nav>

	<div class="jumbotron">
		<div class="container-fluid">
			<tiles:insertAttribute name="body" />
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$(".dropdown").hover(function() {
				$('.dropdown-menu', this).stop(true, true).slideDown("fast");
				$(this).toggleClass('open');
			}, function() {
				$('.dropdown-menu', this).stop(true, true).slideUp("fast");
				$(this).toggleClass('open');
			});
		});
	</script>
</body>