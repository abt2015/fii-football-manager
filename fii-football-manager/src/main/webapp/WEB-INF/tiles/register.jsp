<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../tiles/lib/taglib.jsp"%>
<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/login.js" />"></script>
<script src="js/jquery.js"></script>

<c:choose>
	<c:when test="${param.success eq true}">
		<div class="alert alert-success">Registration successfull!</div>
	</c:when>
	<c:otherwise>

			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-body">
							<div class="col-lg-12">

								<form:form id="register-form" commandName="manager"
									method="POST" enctype="utf8" style="display: block;">

									<div class="form-group">
										<form:input name="username" path="username" class="form-control"
											placeholder="Username" tabindex="2" value="" />
									</div>

									<div class="form-group">
										<form:input name="email" path="email" class="form-control"
											placeholder="Email" tabindex="2" value="" />
										<form:errors path="email" cssClass="alert alert-danger"
											element="div" />
									</div>

									<div class="form-group">
										<form:input name = " password" path="password" class="form-control"
											placeholder="Password" tabindex="2" value="" type="password" />
										<form:errors path="password" cssClass="alert alert-danger"
											element="div" />
									</div>

									<div class="form-group">
										<form:input name = "matchingpassword" path="matchingPassword" class="form-control"
											placeholder="Confirm Password" tabindex="2" value=""
											type="password" />
										<form:errors path="matchingPassword"
											cssClass="alert alert-danger" element="div" />
									</div>


									<div class="row">
										<div class="centered">
											<button type="submit" tabindex="4"
												class="btn btn-lg btn-primary">Register Now</button>
										</div>
									</div>
								</form:form>
							</div>
				</div>
			</div>
		</div>

	</c:otherwise>
</c:choose>
