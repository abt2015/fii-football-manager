<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../tiles/lib/taglib.jsp" %>

<c:if test="${param.success eq 'true' }">
	<div class="alert alert-success">Success</div>
</c:if>

<form:form id="create-player" commandName="player" method="POST"
	enctype="utf8" style="display: block;">

	<div class="form-group">
		<form:input path="firstName" class="form-control"
			placeholder="First Name" tabindex="2" value="" />
	</div>

	<div class="form-group">
		<form:input path="secondName" class="form-control" placeholder="Second Name"
			tabindex="2" value="" />
	</div>

	<div class="form-group">
		<form:input path="alias" class="form-control"
			placeholder="Alias" tabindex="2" value="" />
	</div>
	
	<div class="form-group">
		<form:input path="mainPosition" class="form-control"
			placeholder="Position" tabindex="2" value="" />
	</div>
	<div class="row">
		<div class="centered">
			<button type="submit" tabindex="4" class="btn btn-success">Save</button>
		</div>
	</div>
</form:form>