<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../tiles/lib/taglib.jsp"%>

<c:if test="${param.success eq 'true' }">
	<div class="alert alert-success">Success</div>
</c:if>

<form:form id="create-player" commandName="team" method="POST"
	enctype="utf8" style="display: block;">

	<div class="form-group">
		<form:input path="teamName" class="form-control"
			placeholder="Team name" tabindex="2" value="" />
	</div>

	<div class="form-group">
		<form:input path="teamFans" class="form-control"
			placeholder="Team fans" tabindex="2" value="" />
	</div>

	<div class="form-group">
		<form:select class="form-control" path="league">
			<form:options items="${leagues}"></form:options>
		</form:select>
	</div>

	<div class="form-group">
		<form:input path="buget" class="form-control" placeholder="Buget"
			tabindex="2" value="" />
	</div>
	<div class="row">
		<div class="centered">
			<button type="submit" tabindex="4" class="btn btn-success">Save</button>
		</div>
	</div>
</form:form>