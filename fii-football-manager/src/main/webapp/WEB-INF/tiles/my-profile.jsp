<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../tiles/lib/taglib.jsp"%>

<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}
</style>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>

<c:if test="${param.team eq 'noteam'}">
	<div class="alert alert-warning">You don't manage any team!</div>
</c:if>
<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">${username}</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-4 text-center">
		<div class="panel panel-default">
			<div class="panel-heading">Avatar</div>
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item list-group-item"><img
						src="http://pngimg.com/upload/businessman_PNG6563.png"
						class="img-rounded full-image" width="304" height="236"></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-8 text-center">
		<div class="panel panel-default">
			<div class="panel-heading">Personal Info</div>
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">First Name</span> <b><c:out
								value="${profile.firstName}" /></b><br></li>
					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Second Name</span> <b><c:out
								value="${profile.secondName}" /></b><br></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Favourite Team</span> <b><c:out
								value="${profile.favouriteTeam}" /></b><br></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Managed Team</span> <b><c:out
								value="${team.teamName}" /><br> </b></li>
					<li class="list-unstyled col-lg-10"></li>
					<c:if test="${ gusername eq username }">
						<li class="list-unstyled col-lg-2 text-right">
							<button type="button" class="btn btn-default"
								data-toggle="modal" data-target="#myModal">Edit</button>
						</li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 text-center">
		<div class="panel panel-default">
			<div class="panel-heading">Team Info</div>
			<div class="panel-body">
				<div class="col-lg-6 text-center">
					<ul class="list-group">
						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Team Name</span> <br> <b><c:out
									value="${team.teamName}" /></b></li>
						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Stadium</span><br> <b> <c:out
									value="${team.stadium.stadiumName}" /></b></li>

						<c:if test="${ gusername eq username }">
							<li class="list-unstyled col-lg-2 text-right"><a
								class="btn btn-default"
								href="<spring:url value="/manager/resign.html" />"> Resign </a></li>
						</c:if>
					</ul>
				</div>
				<div class="col-lg-6 text-center">
				
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>

			<div class="modal-body">
				<form:form commandName="mgr-profile" method="POST" enctype="utf8">
					<div class="form-group">

						<label for="firstName"> First Name</label>
						<form:input path="firstName" value="" class="form-control" />

						<label for="secondName"> Second Name</label>
						<form:input path="secondName" value="" class="form-control" />

						<label for="favouriteTeam"> Favourite Team</label>
						<form:input path="favouriteTeam" value="" class="form-control" />

						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save
								changes</button>
						</div>
					</div>
				</form:form>
			</div>

		</div>
	</div>
</div>
<script>
	$('#myModal').on('shown.bs.modal', function() {
		$('#myInput').focus()
	})
</script>