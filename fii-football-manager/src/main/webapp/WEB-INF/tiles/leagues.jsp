<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>
<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}
</style>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>

<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">Leagues</div>
		</div>
	</div>
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr class="info">
				<th>League Name</th>
				<th>League Profile</th>
				<th>League Country</th>
				<th>League Reputation</th>
				<th>Teams Number</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${league}" var="league">
				<tr>
					<td><c:out value="${league.name}" /> <!--</a>  --></td>

					<td><a
						href="<spring:url value="/leagues/${league.leagueId}.html"/>">Visit</a>
					</td>

					<td><c:out value="${league.country}" /></td>

					<td><c:out value="${league.reputation }" /></td>

					<td><c:out value="${league.teamsNo}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>