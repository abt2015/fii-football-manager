<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="./lib/taglib.jsp"%>

<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}
</style>
<div class="container-full fill">
	<div class="panel panel-default">
		<div class="panel-heading">FREE TEAMS</div>
	</div>
	<div id="map">
		<c:if test="${param.success eq false}">
			<div class="alert alert-success">You can't manage two teams!</div>
		</c:if>

		<c:if test="${param.notfree eq 'true'}">
			<div class="alert alert-success">This team has a manager!</div>
		</c:if>

		<table class="table table-bordered table-hover table-striped">
			<thead>
				<tr class="info">
					<th class="active">Team</th>
					<th>League</th>
					<th>Fans Number</th>
					<th>Stadium</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${teams}" var="team">
					<tr>
						<td><a
							href="<spring:url value = "/team/${team.teamId}.html"/>"> <c:out
									value="${team.teamName}" />
						</a></td>

						<td><a
							href="<spring:url value="/leagues/${team.league.leagueId}.html"/>"><c:out
									value="${team.league.name}" /></a></td>
						<td><c:out value="${team.teamFans}" /></td>
						<td><a
							href="<spring:url value="/stadium/${team.stadium.stadiumId}.html"/>"><c:out
									value="${team.stadium.stadiumName}" /></a></td>
						<td><a
							href="<spring:url value="/free-teams/choose/${team.teamId}.html" />">
								Choose </a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>