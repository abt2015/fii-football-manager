<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>

<style>
body {
	padding-top: 5px; /* Whatever the height of your navbar is; the
                             default is 50px */
}
</style>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>

<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">${league.name}</div>
		</div>
	</div>
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr class="info">
				<td>Manager</td>
				<td>Team</td>
				<td>Stadium</td>
				<td>Fans Number</td>
				<td>Action</td>
			</tr>
		</thead>
		<c:forEach items="${league.teams}" var="team">
			<tr>

				<td><a
					href="<spring:url value="/manager/profile/${team.manager.username}.html"/>">
						<c:out value="${team.manager.username}" />
				</a></td>
				<td><a href="<spring:url value="/team/${team.teamId}.html"/>"><c:out
							value="${team.teamName }" /></a></td>
				<td><c:out value="${team.stadium.stadiumName }" /></td>
				<td><c:out value="${team.teamFans }" /></td>
				<td><c:choose>
						<c:when test="${team.teamName eq gteam}"></c:when>
						<c:otherwise>
							<form:form id="challenge-form" commandName="match" method="POST"
								enctype="utf8" style="display: block;">
								<form:input type="hidden" path="team2.teamId"
									value="${team.teamId}" />

								<button type="submit" tabindex="4" class="btn btn-success">Challenge</button>
							</form:form>
						</c:otherwise>
					</c:choose></td>
			</tr>
			<br>
		</c:forEach>

	</table>
</div>
