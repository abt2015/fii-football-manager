<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ include file="./lib/taglib.jsp"%>
<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}
</style>

<c:if test="${gteam eq 'null'}">
	<div class="alert alert-warning">You don't manage a team!</div>
</c:if>

<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">My Squad</div>
		</div>



		<table class="table table-bordered table-hover table-striped">
			<thead>
				<tr class="info">
					<th>Name</th>
					<th>Nationality</th>
					<th>Age</th>
					<th>Main Position</th>
					<th>Team Position</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${squad.contracts}" var="contract" varStatus="i"
					begin="0">
					<tr>
						<td><a
							href="<spring:url value = "/player/${contract.player.playerId}.html"/>">
								<c:out
									value="${contract.player.firstName} ${contract.player.secondName}" />
						</a></td>
						<td><c:out value="${contract.player.nationality}" /></td>
						<td><c:out value="${contract.player.age}" /></td>
						<td><c:out value="${contract.player.mainPosition}" /></td>
						<td><c:out value="${contract.player.teamPosition }" /></td>
					</tr>
				</c:forEach>
				<tr>
					<td><form:form id="train" commandName="team" method="POST">
							<form:select id="train" path="train" items="${skills}" />
							<button type="submit" tabindex="4" class="btn btn-default">Train</button>
						</form:form></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>