<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<style>
@import
	url('http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css')
	;

body {
	padding-top: 15px;
	background-color: #f9f9f9;
}

.carousel-row {
	margin-bottom: 10px;
}

.slide-row {
	padding: 0;
	background-color: #ffffff;
	height: 100px;
	border: 1px solid #e7e7e7;
	overflow: hidden;
	position: relative;
}

.slide-carousel {
	width: 20%;
	float: left;
	display: inline-block;
}

.slide-carousel .carousel-indicators {
	margin-bottom: 0;
	bottom: 0;
	background: rgba(0, 0, 0, .5);
}

.slide-carousel .carousel-indicators li {
	border-radius: 0;
	width: 20px;
	height: 6px;
}

.slide-carousel .carousel-indicators .active {
	margin: 1px;
}

.slide-content {
	position: absolute;
	top: 0;
	left: 20%;
	display: block;
	float: left;
	width: 80%;
	max-height: 76%;
	padding: 1.5% 2% 2% 2%;
	overflow-y: auto;
}

.slide-content h4 {
	margin-bottom: 3px;
	margin-top: 0;
}

.slide-footer {
	position: absolute;
	bottom: 0;
	left: 20%;
	width: 78%;
	height: 20%;
	margin: 1%;
}
</style>

<%@ include file="../tiles/lib/taglib.jsp"%>

<c:forEach items="${challenges}" var="match">
	<div class="row carousel-row">
		<div class="col-xs-8 col-xs-offset-2 slide-row">
			<div id="carousel-1" class="carousel slide slide-carousel"
				data-ride="carousel">
				<div class="carousel-inner">
					<div class="item active">
						<img src="" alt="Image">
					</div>
				</div>
			</div>
			<div class="slide-content">
				<h4>${match.team1.teamName }</h4>
				<h5>${match.team1.league.name }</h5>
			</div>
			<div class="slide-footer">
				<span class="pull-right buttons"> <form:form
						id="accept-challenge" commandName="match" method="POST"
						enctype="utf8" style="display: block;">
						<form:input type="hidden" path="matchId" value="${match.matchId}" />
						<form:input type="hidden" path="challenge" value="1" />
						<button type="submit" tabindex="4" class="btn btn-success">Accept</button>
					</form:form>
				</span> <span class="pull-right buttons"> <form:form
						id="delete-challenge" commandName="match" method="POST">
						<form:input type="hidden" path="matchId" value="${match.matchId}" />
						<form:input type="hidden" path="challenge" value="0" />
						<button type="submit" tabindex="4" class="btn btn-success">Reject</button>
					</form:form>
				</span>
			</div>
		</div>
	</div>
</c:forEach>