<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../tiles/lib/taglib.jsp"%>

<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}

.blank_row {
	height: 10px !important; /* Overwrite any previous rules */
	background-color: #FFFFFF;
}
</style>

<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">${player.firstName}
				${player.secondName}</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 text-center">
			<div class="panel panel-default">
				<div class="panel-heading">Player photo</div>
				<div class="panel-body">
					<ul class="list-group">
						<li class="list-group-item list-group-item"><img
							src="http://2.bp.blogspot.com/-vxJkQIB9fHY/TjjQ3nUV1wI/AAAAAAAAz-M/o3HMrabaWzA/s1600/Wesley-Sneijder-Picture.png"
							class="img-rounded full-image" width="304" height="236"></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-8 text-center">
			<div class="panel panel-default">
				<div class="panel-heading">Player Info</div>
				<div class="panel-body">
					<ul class="list-group">
						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">First Name</span> <b><c:out
									value="${player.firstName}" /></b><br></li>

						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Second Name</span> <b><c:out
									value="${player.secondName}" /></b><br></li>

						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Player Age</span> <b><c:out
									value="${player.age}" /></b><br></li>

						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Player Nationality</span> <b><c:out
									value="${player.nationality}" /><br> </b></li>

						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Main Position</span> <b><c:out
									value="${player.mainPosition}" /><br> </b></li>

						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Main Position</span> <b><c:out
									value="${player.teamPosition}" /><br> </b></li>
						<li class="list-group-item list-group-item-heading text-left li5"><span
							class="badge">Player Value</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 text-center">
		<div class="panel panel-default">
			<div class="panel-heading">Skills</div>
			<div class="panel-body">
				<div class="col-lg-6 text-center responsive">
					<div class="panel panel-default">
						<div class="panel-heading">Outfield Skills</div>
						<div class="panel-body">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr class="active">
										<th>Corner</th>
										<th>Cross</th>
										<th>Dribling</th>
										<th>Finishing</th>
										<th>Free Kick</th>
										<th>Heading</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="${outfield.corners > 70?'success':'warning'}">
											${outfield.corners}</td>
										<td class="${outfield.crossing > 70?'success':'warning'}">
											${outfield.crossing}</td>
										<td class="${outfield.driblling > 70?'success':'warning'}">
											${outfield.driblling}</td>
										<td class="${outfield.finishing > 70?'success':'warning'}">
											${outfield.finishing}</td>
										<td
											class="${outfield.freeKickTaking > 70?'success':'warning'}">
											${outfield.freeKickTaking}</td>
										<td class="${outfield.heading> 70?'success':'warning'}">
											${outfield.heading}</td>
								<thead>
									<tr class="blank_row">
									</tr>
									<tr class="active">
										<th>Long shots</th>
										<th>Long throws</th>
										<th>Marking</th>
										<th>Passing</th>
										<th>Penalty taking</th>
										<th>Tackling</th>
									</tr>
								</thead>
								<tr>
									<td>${outfield.longShots}</td>
									<td>${outfield.longThrows}</td>
									<td>${outfield.marking}</td>
									<td>${outfield.passing}</td>
									<td>${outfield.penaltyTaking}</td>
									<td>${outfield.tackling}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-6 text-center">
					<div class="panel panel-default">
						<div class="panel-heading">Physical skills</div>
						<div class="panel-body">
							<table class="table table-bordered table-hover table-striped">
								<thead>
									<tr class="active">
										<th>Acceleration</th>
										<th>Agility</th>
										<th>Stamina</th>
										<th>Strength</th>
										<th>Jumping</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>${physical.acceleration}</td>
										<td>${physical.agility}</td>
										<td>${physical.stamina}</td>
										<td>${physical.strength}</td>
										<td>${physical.jumping}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-6 text-center responsive">
							<div class="panel panel-default">
								<div class="panel-heading">Mental</div>
								<div class="panel-body">
									<table class="table table-bordered table-hover table-striped">
										<thead>
											<tr class="active">
												<th>Aggresion</th>
												<th>Anticipation</th>
												<th>Creativity</th>
												<th>Positioning</th>
												<th>Teamwork</th>
												<th>Workrate</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>${mental.aggression}</td>
												<td>${mental.anticipation}</td>
												<td>${mental.creativity}</td>
												<td>${mental.positioning}</td>
												<td>${mental.teamwork}</td>
												<td>${mental.workrate}</td>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>