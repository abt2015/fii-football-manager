<%@ include file="./lib/taglib.jsp"%>

<c:url var="firstUrl" value="/players/1.html" />
<c:url var="lastUrl" value="/players/${page.totalPages}.html" />
<c:url var="prevUrl" value="/players/${currentIndex - 1}.html" />
<c:url var="nextUrl" value="/players/${currentIndex + 1}.html" />

<c:if test="${param.success =='true' }">
	<div class="alert alert-success">Success!</div>
</c:if>
<div class="jumbotron vertical-center">

	<div class="container text-center">
		<div class="pagination text-center">
			<div class='pagination pagination-centered'>

				<div class="input-group">
					<span class="input-group-addon">Filter</span> <input id="filter"
						type="text" class="form-control" placeholder="Search by anything">
				</div>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Second Name</th>
							<th>Team</th>
							<th>Team Position</th>
							<th>Value</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody class="searchable">
						<c:forEach items="${players}" var="player">
							<tr>
								<td>${player.firstName}</td>
								<td>${player.secondName}</td>
								<td><c:forEach items="${player.contracts}" var="contract">
										<c:out value="${contract.team.teamName}" />
									</c:forEach></td>
								<td>${player.teamPosition}</td>
								<td></td>
								<td><c:choose>
										<c:when test="${gteam eq 'null' }">

										</c:when>
										<c:when test="${ empty player.contracts}">
											<form:form id="contract-form" commandName="contract"
												method="POST" enctype="utf8" style="display: block;">
												<form:input id="${player.playerId}" type="hidden"
													path="id.playerId" value="${player.playerId}" />

												<button type="submit" tabindex="4" class="btn btn-success">Sign</button>
											</form:form>
										</c:when>
										<c:otherwise>

										</c:otherwise>
									</c:choose></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<ul class="pagination">
					<c:choose>
						<c:when test="${currentIndex == 1}">
							<li class="disabled"><a href="#">&lt;&lt;</a></li>
							<li class="disabled"><a href="#">&lt;</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${firstUrl}">&lt;&lt;</a></li>
							<li><a href="${prevUrl}">&lt;</a></li>
						</c:otherwise>
					</c:choose>
					<c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
						<c:url var="pageUrl" value="/players/${i}.html" />
						<c:choose>
							<c:when test="${i == currentIndex}">
								<li class="active"><a href="${pageUrl}"><c:out
											value="${i}" /></a></li>
							</c:when>
							<c:otherwise>
								<li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:choose>
						<c:when test="${currentIndex == page.totalPages}">
							<li class="disabled"><a href="#">&gt;</a></li>
							<li class="disabled"><a href="#">&gt;&gt;</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${nextUrl}">&gt;</a></li>
							<li><a href="${lastUrl}">&gt;&gt;</a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {

		(function($) {

			$('#filter').keyup(function() {

				var rex = new RegExp($(this).val(), 'i');
				$('.searchable tr').hide();
				$('.searchable tr').filter(function() {
					return rex.test($(this).text());
				}).show();

			})

		}(jQuery));

	});
</script>

