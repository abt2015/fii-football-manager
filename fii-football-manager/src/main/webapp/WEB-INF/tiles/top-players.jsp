<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>
<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th>First name</th>
			<th>Second name</th>
			<th>Team</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${players}" var="player">
			<tr>
				<td><c:out value="${player.firstName}" /></td>
				<td><c:out
						value="${player.secondName}"/></td>
				<td>
				<c:forEach items = "${player.contracts }" var="contract">
					<c:out value="${contract.team.teamName }"/>
				</c:forEach>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>