<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../tiles/lib/taglib.jsp"%>
<form:form commandName="mgr-profile" method="POST" enctype="utf8">
	<br>
	<tr>
		<td><label>First Name: </label></td>
		<td><form:input path="firstName" value="" /></td>
	</tr>
	<br>

	<tr>
		<td><label>Second Name</label></td>
		<td><form:input path="secondName" value="" /></td>
	</tr>
	<br>
	<tr>
		<td><label>Favourite Team</label></td>
		<td><form:input path="favouriteTeam" value="" /></td>
	</tr>
	<br>
	<button type="submit">Save</button>
</form:form>

