<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>


<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 8px;
}
</style>

<c:if test="${param.exist eq 'not' }">
	<div class="alert alert-warning">No team with this ID.</div>
</c:if>
<c:if test="${param.success eq true}">
	<div class="alert alert-success">Your offer was sent!</div>
</c:if>

<c:if test="${gteam eq 'null'}">
	<div class="alert alert-warning">Remind that, you don't manage a
		team and you can't do transfers!</div>
</c:if>
<div class="container-full">
	<div class="col-lg-14 text-left">
		<ul class="list-group">
			<li class="list-group-item list-group-item active li5">
				<h4 class="list-group-item-heading">
					<c:out value="${team.teamName}" />
				</h4>
			</li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-lg-4 text-center">
		<div class="panel panel-default">
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item list-group-item active li5">
						<h4 class="list-group-item-heading">Team Logo</h4>
					</li>
					<li class="list-group-item list-group-item"><img
						src="cinqueterre.jpg" class="img-rounded full-image" width="304"
						height="236"></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-lg-8 text-center">
		<div class="panel panel-default">
			<div class="panel-body">
				<ul class="list-group">

					<li class="list-group-item list-group-item active li5">
						<h4 class="list-group-item-heading">Team Info</h4>
					</li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Team Name</span> <b><c:out
								value="${team.teamName}" /></b></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">League</span> <b><c:out
								value="${team.league.name}" /></b></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Fans Number</span> <b><c:out
								value="${team.teamFans}" /></b></li>
				</ul>

				<br>
				<ul class="list-group">

					<li class="list-group-item list-group-item active li5">
						<h4 class="list-group-item-heading">Stadium Info</h4>
					</li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Stadium Name</span> <b><c:out
								value="${team.stadium.stadiumName}" /></b><br></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Capacity</span> <b><c:out
								value="${team.stadium.capacity}" /><br></b></li>

					<li class="list-group-item list-group-item-heading text-left li5"><span
						class="badge">Build Date</span> <b><c:out
								value="${team.stadium.buildDate}" /><br></b></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 text-center">
		<div class="panel panel-default">
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item list-group-item active li5">
						<h4 class="list-group-item-heading">${team.teamName }'s
							Players</h4>
					</li>
				</ul>
				<div class="table-responsive">
					<table class="table ">
						<thead>
							<tr>
								<th>Player Name</th>
								<th>Nationality</th>
								<th>Position</th>
								<th>Player Value</th>

								<c:if test="${mgr_team != 'null' and mgr_team == 'other'}">
									<th>Action</th>
								</c:if>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${team.contracts}" var="contract">
								<tr>
									<td><a
										href="<spring:url value = "/player/${contract.player.playerId}.html"/>">
											<c:out value="${contract.player.firstName}" /> <c:out
												value="${contract.player.secondName}" />
									</a></td>
									<td><c:out value="${contract.player.nationality}" /></td>
									<td><c:out value="${contract.player.mainPosition}" /></td>
									<td></td>
									<c:if test="${mgr_team != 'null' and mgr_team == 'other'}">
										<c:choose>
											<c:when test="${gbuget > 0 }">
												<td><form:form id="transfer-form"
														commandName="transfer-offer" method="POST">
														<div id="slider">
															<form:input class="bar" type="range" id="rangeinput"
																name="points" path="price"
																onchange="rangevalue.value=value" value="0" min="0"
																max="${gbuget}" />
															<output id="rangevalue">0
															</output>
															<span class="glyphicon glyphicon-euro"></span>
														</div>

														<form:input type="hidden" path="player_id"
															value="${contract.player.playerId}" />
														<button type="submit" class="btn btn-default">Send
															Offer</button>
													</form:form></td>
											</c:when>
											<c:otherwise>
												<td>No money</td>
											</c:otherwise>
										</c:choose>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function showValue(newValue) {
		document.getElementById("range_value").innerHTML = newValue;
	}
</script>