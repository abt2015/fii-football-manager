<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>

<jsp:useBean id="today" class="java.util.Date" scope="request" />

<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}

.table-responsive {
	overflow-x: auto;
}

.hide {
	display: none;
}

.table-field {
	background-color: green;
	border-top: none !important;
	border-bottom: none !important;
}

.table-field>thead>tr>th, .table-field>tbody>tr>th, .table-field>tfoot>tr>th,
	.table-field>thead>tr>td, .table-field>tbody>tr>td, .table-field>tfoot>tr>td
	{
	border-top: none;
}
</style>
<c:if test="${gteam eq 'null'}">
	<div class="alert alert-warning">You don't manage a team!</div>
</c:if>

<div class="row">
	<div class="container-full">
		<div class="col-lg-14 text-left">
			<ul class="list-group">
				<li class="list-group-item list-group-item active li5">
					<h4 class="list-group-item-heading">My Tactics</h4>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="row">
	<div class="container-full">
		<div class="col-lg-6 text-left">
			<ul>
				<li>${tactic}</li>
			</ul>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-7 text-left">
		<table
			class="table table-bordered table-hover table-striped table-responsive">
			<thead>
				<tr class="info">
					<th>Team Position</th>
					<th>Name</th>
					<th>Nationality</th>
					<th>Age</th>
					<th>Skill</th>
					<th>Main Position</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${squad.contracts}" var="contract">
					<tr>
						<td>"${contract.player.teamPosition}"</td>
						<td><a
							href="<spring:url value = "/player/${contract.player.playerId}.html"/>">
								<c:out
									value="${contract.player.firstName} ${contract.player.secondName}" />
						</a></td>
						<td><c:out value="${contract.player.nationality}" /></td>
						<td><c:out value="${contract.player.dateOfBirth}" /></td>
						<td><c:out value="" /></td>
						<td><c:out value="${contract.player.mainPosition}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>

	<c:set var="GK" value="false" />
	<c:set var="LB" value="false" />
	<c:set var="CB1" value="false" />
	<c:set var="CB2" value="false" />
	<c:set var="RB" value="false" />
	<c:set var="LWB" value="false" />
	<c:set var="CDM" value="false" />
	<c:set var="RWB" value="false" />
	<c:set var="LM" value="false" />
	<c:set var="CM1" value="false" />
	<c:set var="CM2" value="false" />
	<c:set var="RM" value="false" />
	<c:set var="LAM" value="false" />
	<c:set var="CAM1" value="false" />
	<c:set var="CAM2" value="false" />
	<c:set var="RAM" value="false" />
	<c:set var="LF" value="false" />
	<c:set var="CF1" value="false" />
	<c:set var="CF2" value="false" />
	<c:set var="RF" value="false" />

	<c:forEach var="post" items="${posts}">
		<c:if test="${post eq 'GK'}">
			<c:set var="GK" value="true" />
		</c:if>
		<c:if test="${post eq 'LB'}">
			<c:set var="LB" value="true" />
		</c:if>
		<c:if test="${post eq 'CB1'}">
			<c:set var="CB1" value="true" />
		</c:if>
		<c:if test="${post eq 'CB2'}">
			<c:set var="CB2" value="true" />
		</c:if>
		<c:if test="${post eq 'RB'}">
			<c:set var="RB" value="true" />
		</c:if>
		<c:if test="${post eq 'LWB'}">
			<c:set var="LWB" value="true" />
		</c:if>
		<c:if test="${post eq 'CDM1'}">
			<c:set var="CDM1" value="true" />
		</c:if>
		<c:if test="${post eq 'CDM2'}">
			<c:set var="CDM2" value="true" />
		</c:if>
		<c:if test="${post eq 'RWB'}">
			<c:set var="RWB" value="true" />
		</c:if>
		<c:if test="${post eq 'LM'}">
			<c:set var="LM" value="true" />
		</c:if>
		<c:if test="${post eq 'CM1'}">
			<c:set var="CM1" value="true" />
		</c:if>
		<c:if test="${post eq 'CM2'}">
			<c:set var="CM2" value="true" />
		</c:if>
		<c:if test="${post eq 'RM'}">
			<c:set var="RM" value="true" />
		</c:if>
		<c:if test="${post eq 'LAM'}">
			<c:set var="LAM" value="true" />
		</c:if>
		<c:if test="${post eq 'CAM1'}">
			<c:set var="CAM1" value="true" />
		</c:if>
		<c:if test="${post eq 'CAM2'}">
			<c:set var="CAM2" value="true" />
		</c:if>
		<c:if test="${post eq 'RAM'}">
			<c:set var="RAM" value="true" />
		</c:if>
		<c:if test="${post eq 'LF'}">
			<c:set var="LF" value="true" />
		</c:if>
		<c:if test="${post eq 'CF1'}">
			<c:set var="CF1" value="true" />
		</c:if>
		<c:if test="${post eq 'CF2'}">
			<c:set var="CF2" value="true" />
		</c:if>
		<c:if test="${post eq 'RF'}">
			<c:set var="RF" value="true" />
		</c:if>
	</c:forEach>
	
	<form method="POST" id="tactic"></form>
	<div class="col-lg-5 text-left">
		<table class="table table-field text-right unborder">
			<thead>
				<tr class="info">
					<th></th>
					<th></th>
					<th>Tactic</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>${RF eq true ? 'RF' : '' }</td>
					<td>${CF1 eq true ? 'CF' : '' }
						
					</td>
					<td>${CF2 eq true ? 'CF' : '' }</td>
					<td>${LF eq true ? 'LF' : '' }</td>
				</tr>
				<tr>
					<td>${RAM eq true ? 'RAM' : ''}</td>
					<td>${CAM1 eq true ? ' CAM' : '' }</td>
					<td>${CAM2 eq true ? ' CAM' : ''}</td>
					<td>${LAM eq true ? 'LAM' : ''}</td>
				</tr>
				<tr>
					<td>${RM eq true ? 'RM' : '' }</td>
					<td>${CM1 eq true ? 'CM' : ''}</td>
					<td>${CM2 eq true ? 'CM' : ''}</td>
					<td>${LM eq true ? 'LM' : '' }</td>
				</tr>

				<tr>
					<td>${RWB eq true ? 'RWB' : '' }</td>
					<td>${CDM1 eq true ? 'CDM' : '' }</td>
					<td>${CDM2 eq true ? 'CDM' : '' }</td>
					<td>${LWB eq true ? 'LWB' : '' }</td>
				</tr>
				<tr>
					<td>${RB eq true ? 'RB' : '' }</td>
					<td>${CB1 eq true ? 'CB' : '' }</td>
					<td>${CB2 eq true ? 'CB' : '' }</td>
					<td>${LB eq true ? 'LB' : '' }</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td class="text-left">${GK eq true ? 'GK' : '' }</td>
					<td></td>
				</tr>
			</tbody>
		</table>

	</div>
</div>
