<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="./lib/taglib.jsp"%>
<h1>Stadium profile</h1>
<table class="table">
	<thead>
		<tr>
			<th>Stadium name</th>
			<th>Capacity</th>
			<th>Build Date</th>
			<th>Teams</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><c:out value="${stadium.stadiumName}" />
			<td><c:out value="${stadium.capacity}" /></td>
			<td><c:out value="${stadium.buildDate}" /></td>
			<td><c:forEach items="${stadium.teams}" var="team">
					<c:out value="${team.teamName}" />
				</c:forEach></td>
		</tr>
	</tbody>
</table>

