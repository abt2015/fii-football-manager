<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="./lib/taglib.jsp"%>
<style>
body {
	padding-top: 15px; /* Whatever the height of your navbar is; the
                             default is 50px */
}

.full-image {
	display: block;
	width: 100%;
	margin: 0 auto;
}

.li5 {
	padding: 5px;
}
</style>
<div class="container-full">
	<div class="col-lg-14 text-left">
		<div class="panel panel-default">
			<div class="panel-heading">My Offers</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${gteam == 'null'}">
			<div class="alert alert-warning">You don't manage a team!</div>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${offersNo eq '0'}">
					<div class="alert alert-success">No offers!</div>
				</c:when>
				<c:otherwise>
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>Player</th>
								<th>Price</th>
								<th>Transfer Status</th>
								<th>From</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${myoffers}" var="offer">
								<tr>
									<td><c:out value="${offer.player.firstName}" /> <c:out
											value="${offer.player.secondName}" /></td>
									<td><c:out value="${offer.price}" /><span
										class="glyphicon glyphicon-euro"></span></td>
									<td><c:out value="${offer.accepted}"></c:out>
									<td><c:out value="${offer.team.teamName}" /></td>
									<td><form:form id="offer-form-${offer.player.playerId}"
											commandName="accepted-offer" method="POST">
											<form:input path="accepted" type="hidden" value="1" />
											<form:input type="hidden" path="offerId"
												value="${offer.offerId}" />
											<button type="submit" class="btn btn-default">Accept
												Offer</button>
										</form:form></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</div>