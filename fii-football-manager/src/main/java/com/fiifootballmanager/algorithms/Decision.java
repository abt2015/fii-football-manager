package com.fiifootballmanager.algorithms;

public enum Decision {
	PASS, SHOT, DRIBBLE, TACKLE, FAULT, CROSS
}
