package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Physical;

public interface PhysicalRepository extends JpaRepository<Physical, Long> {

}
