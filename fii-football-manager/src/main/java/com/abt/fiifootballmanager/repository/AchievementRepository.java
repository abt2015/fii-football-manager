package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Achievement;

public interface AchievementRepository extends JpaRepository<Achievement, Long> {

}
