package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
	
	@Query("select m from Manager m where m.username = :username")
	Manager findOne(@Param("username") String username);
	
	@Query("select m from Manager m where m.email = :email")
	Manager findOneByEmail(@Param("email") String email);
	
}
