package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	
	@Query("select r from Role r where r.role = :roleName")
	Role findOne(@Param("roleName") String roleName);

}
