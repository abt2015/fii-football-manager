package com.abt.fiifootballmanager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.Team;

public interface TeamRepository extends JpaRepository<Team,Long>{
	
	@Query("select t from Team t where manager_id is null")
	List<Team> getFreeTeams();
	
	@Query("select t from Team t where manager_id = :manager_id")
	Team findOneByManagerId(@Param("manager_id") long manager_id);
	
	@Query("select t from Team t where team_name = :teamName")
	Team findOneByName(@Param("teamName") String teamName);	
}
