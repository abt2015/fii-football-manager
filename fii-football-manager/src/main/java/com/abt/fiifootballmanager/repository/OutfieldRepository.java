package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Outfield;

public interface OutfieldRepository extends JpaRepository<Outfield, Long> {

}
