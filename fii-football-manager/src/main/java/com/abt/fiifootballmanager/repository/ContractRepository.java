package com.abt.fiifootballmanager.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.Contract;

public interface ContractRepository extends JpaRepository<Contract, Long> {
	
	@Query("SELECT c FROM Contract c WHERE team_id = :teamId")
	List<Contract> findOneTeamContracts(@Param("teamId") long teamId);
	
	@Modifying  
	@Transactional
	@Query("DELETE FROM Contract c WHERE player_id = :playerId")
	Object deletePlayerContract(@Param("playerId") long playerId);
	
	@Query("SELECT c FROM Contract c WHERE player_id = :playerId")
	Contract findByPlayerId(@Param("playerId") long playerId);

}
