package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;

import com.abt.fiifootballmanager.entity.League;

public interface LeagueRepository extends JpaRepository<League, Long> {
	
	@Query("select l from League l where l.name = :league_name")
	League findOneByName(@PathVariable("league_name") String league_name);

	League findOne(Long id);

}
