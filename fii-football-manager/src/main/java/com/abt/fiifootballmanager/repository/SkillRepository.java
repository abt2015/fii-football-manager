package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {

}
