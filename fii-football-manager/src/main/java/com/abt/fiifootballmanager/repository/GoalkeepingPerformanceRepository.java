package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.GoalkeepingPerformance;

public interface GoalkeepingPerformanceRepository extends
		JpaRepository<GoalkeepingPerformance, Long> {

}
