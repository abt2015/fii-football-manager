package com.abt.fiifootballmanager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.TransferOffer;

public interface TransferOfferRepository extends JpaRepository<TransferOffer, Long>{
	
	@Query("SELECT to FROM TransferOffer to, Contract c "
			+ "WHERE c.team.teamId = :teamid AND "
			+ "to.player.playerId = c.player.playerId")
	List<TransferOffer> getMyOffers(@Param("teamid") long teamid);
	
	@Query("SELECT to FROM TransferOffer to WHERE to.player.playerId = :playerId")
	List<TransferOffer> findOnePlayerOffers(@Param("playerId") long playerId);

}
