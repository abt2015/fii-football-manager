package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Match;


public interface MatchRepository extends JpaRepository<Match,Long>{
	
}
