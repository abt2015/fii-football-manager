package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Mental;

public interface MentalRepository extends JpaRepository<Mental,Long>{

}
