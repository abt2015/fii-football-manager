package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Performance;

public interface PerformanceRepository extends JpaRepository<Performance, Long> {

}
