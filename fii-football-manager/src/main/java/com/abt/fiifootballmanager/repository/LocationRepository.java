package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Long> {

}
