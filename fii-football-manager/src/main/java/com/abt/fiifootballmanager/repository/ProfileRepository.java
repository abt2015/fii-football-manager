package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abt.fiifootballmanager.entity.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
	
	@Query("select p from Profile p where MANAGER_ID = :id")
	Profile findOneWithManagerId(@Param("id") long id);
	
	@Query("select p from Profile p where username = :username")
	Profile findOneWithUsername(@Param("username") String username);

}
