package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abt.fiifootballmanager.entity.Stadium;

public interface StadiumRepository extends JpaRepository<Stadium, Long> {

}
