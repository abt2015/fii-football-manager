package com.abt.fiifootballmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.abt.fiifootballmanager.entity.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {
	
	@Query("SELECT max(p.playerId) FROM Player p")
	int findMax();
	
}
