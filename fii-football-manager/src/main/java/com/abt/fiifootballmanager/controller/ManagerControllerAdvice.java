package com.abt.fiifootballmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Match;
import com.abt.fiifootballmanager.entity.TransferOffer;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.TeamService;
import com.abt.fiifootballmanager.service.TransferOfferService;

@ControllerAdvice
public class ManagerControllerAdvice {

	@Autowired
	private ManagerService managerService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private TransferOfferService transferService;

	@ModelAttribute
	public void addBugetToModel(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {

			String username = auth.getName();
			Manager mgr = managerService.findOne(username);
			model.addAttribute("gusername", username);
			
			int notif = 0;
			if (mgr.getTeam() != null) {
				model.addAttribute("gteam", mgr.getTeam().getTeamName());
				model.addAttribute("gbuget", mgr.getTeam().getBuget());
				
				List<Match> matches = mgr.getTeam().getMatches2();
				model.addAttribute("gmatches", matches.size());
				notif = notif + matches.size();
				List<TransferOffer> allOffers = transferService
						.getMyOffers(mgr.getTeam().getTeamId());
				
				if (!allOffers.isEmpty()) {
					model.addAttribute("offersNo", allOffers.size());
					notif = notif + allOffers.size();
				} else
					model.addAttribute("offersNo", 0);
			}
			else 
				model.addAttribute("gteam", "null");
			model.addAttribute("notif", notif);
		}

	}
}
