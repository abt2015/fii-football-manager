package com.abt.fiifootballmanager.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.exceptions.EmailExistsException;
import com.abt.fiifootballmanager.service.ManagerService;

@Controller
public class RegisterController {

	@Autowired
	private ManagerService managerService;

	@ModelAttribute("manager")
	public Manager createManager() {
		return new Manager();
	}

	@RequestMapping("/register")
	public String register() {
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String saveManager(
			@ModelAttribute("manager") @Valid Manager manager,
			BindingResult result, Errors error) {

		if (!result.hasErrors()) {
			try {
				managerService.registerNewManager(manager);
			} catch (EmailExistsException e) {
				e.printStackTrace();
			}
			return "redirect:/register.html?success=true";
		}

		return "register";

	}
}
