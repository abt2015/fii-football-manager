package com.abt.fiifootballmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Profile;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.service.ContractService;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.PlayerService;
import com.abt.fiifootballmanager.service.ProfileService;
import com.abt.fiifootballmanager.service.TeamService;
import com.abt.fiifootballmanager.service.TransferOfferService;

@Controller
public class ManagerController {

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private PlayerService playerService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private TransferOfferService transferService;

	@ModelAttribute("mgr-profile")
	public Profile createProfile() {
		return new Profile();
	}

	@RequestMapping("/me.html")
	public String managerSidebar() {
		return "authenticated";
	}

	@RequestMapping("/manager/profile/{username}.html")
	public String managerProfile(Model model, Model team,
			@PathVariable String username) {
		String userName = username;
		Manager manager = managerService.findOne(userName);
		team.addAttribute("team",
				teamService.getOneByManager(manager.getManagerId()));
		model.addAttribute("profile",
				profileService.findProfileByUsername(username));
		return "my-profile";
	}

	@RequestMapping("/manager/my-profile")
	public String authenticatedManagerProfile(Model model, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String userName = auth.getName();
		Profile profile = profileService.findProfileByUsername(userName);
		model.addAttribute("username", userName);
		model.addAttribute("profile", profile);
		Manager manager = managerService.findOne(userName);
		model.addAttribute("team",
				teamService.getOneByManager(manager.getManagerId()));
		return "my-profile";
	}

	@RequestMapping(value = "/manager/resign", method = RequestMethod.GET)
	public String resign(Model model, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String userName = auth.getName();
		Manager mgr = managerService.findOne(userName);
		if (mgr.getTeam() != null) {
			Team mgr_team = teamService.getOneByManager(mgr.getManagerId());
			mgr_team.setManager(null);
			teamService.save(mgr_team);
		}
		else
			return "redirect:/manager/my-profile.html?team=noteam";
		return "my-profile";
	}

	@RequestMapping("/manager/my-profile/edit")
	public String profileEditorView() {
		return "edit-profile";
	}

	@RequestMapping(value = { "/manager/my-profile/edit", "/manager/my-profile" }, method = RequestMethod.POST)
	public String editMyProfile(@ModelAttribute("mgr-profile") Profile profile, RedirectAttributes ra,
			BindingResult result) {
		if (!result.hasErrors()) {
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			String userName = auth.getName();
			profile.setUsername(userName);
			profileService.save(profile);
			return "redirect:/manager/my-profile.html";
		}
		return "edit-profile";
	}
	
}
