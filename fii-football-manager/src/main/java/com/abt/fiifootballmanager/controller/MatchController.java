package com.abt.fiifootballmanager.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Match;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.MatchService;

@Controller
public class MatchController {
	
	@Autowired
	private MatchService matchService;
	
	@Autowired
	private ManagerService managerService;
	
	@RequestMapping(value = "/team/challenges", method = RequestMethod.GET)
	public String challenges(Model model){
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = mgr.getTeam();
		if(mgr_team != null){
			List<Match> challenges = mgr_team.getMatches2();
			for(int i = 0; i < challenges.size(); i++){
				if(challenges.get(i).getChallenge() != 0)
					challenges.remove(i);
			}
			model.addAttribute("challenges", challenges);
		}		
		return "challenges";
	}
	
	@ModelAttribute("match")
	public Match newMatch(){
		return new Match();
	}
	@RequestMapping(value = "/team/challenges", method = RequestMethod.POST)
	public String acceptChallenge(HttpServletResponse response, @ModelAttribute("match") Match match){
		
		int accepted = match.getChallenge();
		switch(accepted){
			case 1:{
				Match acceptedMatch = matchService.getMatch(match.getMatchId());
				acceptedMatch.setChallenge(1);
				matchService.saveMatch(acceptedMatch);
				matchService.simulateMatch(response, acceptedMatch);
				return "redirect:/team/challenges.html";
			}
			case 0: {
				Match acceptedMatch = matchService.getMatch(match.getMatchId());
				acceptedMatch.setChallenge(0);
				matchService.deleteMatch(match);
				return "redirect:/team/challenges.html";	
			}
		}
				
		return "challenges";
	}
	
}
