package com.abt.fiifootballmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.abt.fiifootballmanager.entity.Contract;
import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.service.ContractService;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.PlayerService;
import com.abt.fiifootballmanager.service.TeamService;

@Controller
public class PlayerController {
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired 
	private ManagerService managerService;
	
	@Autowired
	private ContractService contractService;
	
	@RequestMapping("/top-players")
	public String topPlayers(Model model){
		model.addAttribute("players", playerService.findAll());
		return "top-players";
	}
	
	@RequestMapping(value = "/player/{playerId}", method = RequestMethod.GET)
	public String playerProfile(@PathVariable("playerId") long playerId, Model model){
		Player player = playerService.findByPlayerId(playerId);
		model.addAttribute("player", player);
		model.addAttribute("mental", player.getSkill().getMental());
		model.addAttribute("physical", player.getSkill().getPhysical());
		model.addAttribute("outfield", player.getSkill().getOutfield());

		return "player-profile";
	}
	
	@RequestMapping(value = "/players/{pageNumber}", method = RequestMethod.GET)
	public String getPlayersPage(@PathVariable Integer pageNumber, Model model) {
	    Page<Player> page = playerService.getPlayers(pageNumber);

	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + 10, page.getTotalPages());
	    
	    List<Player> players = page.getContent();
	    
	    model.addAttribute("players", players);
	    model.addAttribute("page", page);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("currentIndex", current);

	    return "all-players";
	}
	
	@ModelAttribute("contract")
	public Contract newContract(){
		return new Contract();
	}
	
	@RequestMapping(value = "/player/1.html")
	public String signPlayer(){
		return "exception";
	}
	
	@RequestMapping(value = "/players/{pageNumber}", method = RequestMethod.POST)
	public String sign(@ModelAttribute("contract") Contract newContract, BindingResult result, RedirectAttributes ra){
		
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		
		String userName = auth.getName();
		Manager mgr = managerService.findOne(userName);
		Team team = mgr.getTeam();
		newContract.getId().setTeamId(team.getTeamId());
		contractService.save(newContract);
		return "redirect:/players/1.html?success=true";
	}
}
