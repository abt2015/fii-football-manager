package com.abt.fiifootballmanager.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.abt.fiifootballmanager.entity.Formation;
import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.entity.SkillsEnum;
import com.abt.fiifootballmanager.entity.Tactic;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.entity.TransferOffer;
import com.abt.fiifootballmanager.service.ContractService;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.MatchService;
import com.abt.fiifootballmanager.service.PlayerService;
import com.abt.fiifootballmanager.service.TeamService;
import com.abt.fiifootballmanager.service.TransferOfferService;

@Controller
public class TeamController {

	@Autowired
	private ManagerService managerService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private TransferOfferService transferService;

	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private MatchService matchService;
	
	@RequestMapping(value = "/free-teams")
	public String getFreeTeams(Model model) {
		model.addAttribute("teams", teamService.getFreeTeams());
		return "free-teams";
	}

	@RequestMapping(value = "/free-teams/choose/{team_id}.html")
	public String chooseTeam(@PathVariable long team_id, RedirectAttributes ra) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Team team = teamService.getOne(team_id);

		if (team.getManager() != null) {

			return "redirect:/free-teams.html?notfree=true";
		} else {
			Manager manager = managerService.findOne(auth.getName());
			Team test = teamService.getOneByManager(manager.getManagerId());

			if (test != null) {
				return "redirect:/free-teams.html?success=false";
			}

			team.setManager(manager);
			teamService.save(team);
			return "redirect:/manager/my-profile.html";
		}
	}

	@RequestMapping(value = "/team/{teamid}.html", method = RequestMethod.GET)
	public String teamDetails(@PathVariable("teamid") long teamId, Model model) {
		Team team = teamService.getOne(teamId);
		if (team == null) {
			model.addAttribute("exist", "not");
		} else {
			model.addAttribute("team", team);

			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			Manager mgr = managerService.findOne(auth.getName());
			Team mgr_team = mgr.getTeam();

			if (team.getManager() != null)
				if (mgr_team == null)
					model.addAttribute("mgr_team", "null");
				else if (!team.getManager().equals(mgr))
					model.addAttribute("mgr_team", "other");
		}
		return "team-profile";
	}

	@ModelAttribute("transfer-offer")
	public TransferOffer transferOffer() {
		return new TransferOffer();
	}

	@RequestMapping(value = "/team/{teamid}.html", method = RequestMethod.POST)
	public String transferPlayer(
			@ModelAttribute("transfer-offer") TransferOffer offer,
			BindingResult result, @PathVariable("teamid") long teamid,
			Model model) {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = mgr.getTeam();
		if (mgr_team == null) {
			return "team-profile";
		} else if (!result.hasErrors()) {

			Player player = playerService.findByPlayerId(offer.getPlayer_id());
			offer.setTeam(mgr_team);
			offer.setPlayer(player);
			offer.setAccepted(0);
			transferService.save(offer);
			return "redirect:/team/" + teamid + ".html?success=true";
		}
		return "team-profile";
	}

	@RequestMapping(value = "/team/squad")
	public String teamSquad(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = teamService.getOneByManager(mgr.getManagerId());
		model.addAttribute("squad", mgr_team);
		
		List<String> skills = new ArrayList<String>();
		for(SkillsEnum se : SkillsEnum.values())
			skills.add(se.toString());
		model.addAttribute("skills", skills);
		return "squad-view";
	}
	
	@ModelAttribute("player")
	public Player createPlayer(){
		return new Player();
	}
	
	@ModelAttribute("team")
	public Team updateTeam(){
		return new Team();
	}
	@RequestMapping(value = "/team/squad", method = RequestMethod.POST)
	public String squadTraining(@ModelAttribute("team") Team team){
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = mgr.getTeam();
		mgr_team.setTrain(team.getTrain());
		teamService.trainTeam(mgr_team);
		return "squad-view";
	}
	@RequestMapping(value = "/team/formation", method = RequestMethod.GET)
	public String teamFormation(Model model) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = teamService.getOneByManager(mgr.getManagerId());
		model.addAttribute("squad", mgr_team);

		List<Player> players = new ArrayList<Player>();
		for (int i = 0; i < mgr_team.getContracts().size(); i++) {
			players.add(mgr_team.getContracts().get(i).getPlayer());
		}

		Formation formation = new Formation();
		for (Tactic t : Tactic.values())
			if (t.getTactic().equals(mgr_team.getTactic()))
				formation.setTactic(t);
		model.addAttribute("posts", formation.getPositions());
		model.addAttribute("tactic", formation.getTactic().getTactic()
				.toString());
		model.addAttribute("players", players);
		return "formation-view";
	}

	@RequestMapping(value = "/team/formation", method = RequestMethod.POST)
	public String setFormation(@ModelAttribute("players") Player players) {
		return "formation-view";
	}

}
