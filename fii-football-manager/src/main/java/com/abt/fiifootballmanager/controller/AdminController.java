package com.abt.fiifootballmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.abt.fiifootballmanager.entity.League;
import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.repository.PlayerRepository;
import com.abt.fiifootballmanager.service.LeagueService;
import com.abt.fiifootballmanager.service.TeamService;

@Controller
public class AdminController {

	@Autowired
	private PlayerRepository playerService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private LeagueService leagueService;

	@ModelAttribute("player")
	public Player createPlayer() {
		return new Player();
	}

	@RequestMapping(value = "/admin-side/add-player")
	public String getPlayerForm() {
		return "addplayer";
	}

	@RequestMapping(value = "/admin-side/add-player", method = RequestMethod.POST)
	public String addPlayer(@ModelAttribute("player") Player player,
			BindingResult result, RedirectAttributes ra) {
		if (!result.hasErrors()) {
			player.setPlayerId(playerService.findMax() + 1);
			playerService.save(player);
			return "redirect:/admin-side/add-player.html?success=true";
		}
		return "addplayer";
	}

	@RequestMapping(value = "/admin-side/add-team")
	public String getTeamForm(Model model) {
		List<League> leagues = leagueService.findAll();
		model.addAttribute("leagues", leagues);
		return "addteam";
	}

	@ModelAttribute("team")
	public Team createTeam() {
		return new Team();
	}

	@RequestMapping(value = "/admin-side/add-team", method = RequestMethod.POST)
	public String addTeam(@ModelAttribute("team") Team team,
			BindingResult result, RedirectAttributes ra) {
		teamService.save(team);
		return "redirect:/admin-side/add-team.html?success=true";

	}

	@RequestMapping(value = "/admin-side/update-teams", method = RequestMethod.GET)
	public String udpdateTeams() {
		return "update-teams";
	}
}
