package com.abt.fiifootballmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.abt.fiifootballmanager.service.StadiumService;

@Controller
public class StadiumController {
	
	@Autowired
	private StadiumService stadiumService;
	
	@RequestMapping("/stadium/{stadium_id}.html")
	public String getStadium(Model model, @PathVariable("stadium_id") long stadium_id){
		model.addAttribute("stadium", stadiumService.findById(stadium_id));
		return "stadium-profile";
	}
}
