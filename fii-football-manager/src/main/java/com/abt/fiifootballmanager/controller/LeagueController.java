package com.abt.fiifootballmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.abt.fiifootballmanager.entity.League;
import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Match;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.service.LeagueService;
import com.abt.fiifootballmanager.service.ManagerService;
import com.abt.fiifootballmanager.service.MatchService;

@Controller
public class LeagueController {
	
	@Autowired
	private LeagueService leagueService;
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private MatchService matchService;
	
	
	@RequestMapping("/leagues")
	public String returnLeagues(Model model){
		model.addAttribute("league", leagueService.getAllLeagues());
		return "leagues";
	}
	
	@RequestMapping(value = "/leagues/{id}.html", method = RequestMethod.GET)
	public String chooseTeam(@PathVariable Long id, Model model) {
		League league = leagueService.findOne(id);
		model.addAttribute("league", league);
		return "league-profile";
	}
	
	@ModelAttribute("match")
	public Match newMatch(){
		return new Match();
	}
	@RequestMapping(value = "/leagues/{id}.html", method = RequestMethod.POST)
	public String challenge(@ModelAttribute("match") Match match, BindingResult result){
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		
		Manager mgr = managerService.findOne(auth.getName());
		Team mgr_team = mgr.getTeam();
		
		match.setTeam1(mgr_team);
		match.setChallenge(0);
		
		matchService.saveMatch(match);
		
		return "league-profile";
	}
}
