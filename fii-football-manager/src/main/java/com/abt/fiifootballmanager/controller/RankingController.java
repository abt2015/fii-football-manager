package com.abt.fiifootballmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.abt.fiifootballmanager.entity.League;
import com.abt.fiifootballmanager.entity.Ranking;
import com.abt.fiifootballmanager.entity.RankingLine;
import com.abt.fiifootballmanager.service.LeagueService;

@Controller
public class RankingController {
	
	@Autowired
	private LeagueService leagueService;
	
	@RequestMapping("/league/{id}/ranking.html")
	public String getRank(@PathVariable("id") long league_id, Model model){
		League league = leagueService.findOne(league_id);
		Ranking ranking = new Ranking(league);
		List<RankingLine> lines = ranking.getRows();
		model.addAttribute("lines", lines);
		return "ranking-view";
	}
}
