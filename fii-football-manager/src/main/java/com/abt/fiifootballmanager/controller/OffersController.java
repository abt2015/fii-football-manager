package com.abt.fiifootballmanager.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.abt.fiifootballmanager.entity.Contract;
import com.abt.fiifootballmanager.entity.ContractPK;
import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.entity.TransferOffer;
import com.abt.fiifootballmanager.repository.ManagerRepository;
import com.abt.fiifootballmanager.service.ContractService;
import com.abt.fiifootballmanager.service.TeamService;
import com.abt.fiifootballmanager.service.TransferOfferService;

@Controller
public class OffersController {
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private TransferOfferService transferService;
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private ManagerRepository managerRepository;
	
	@RequestMapping("/transfers**")
	public String offers(){
		return "my-offers";
	}
	
	@RequestMapping(value="/transfers/offers", method= RequestMethod.GET)
	public String myOffers(Model model){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		Manager mgr = managerRepository.findOne(username);
		Team mgrTeam = mgr.getTeam();
		if(mgrTeam == null){
			model.addAttribute("noTeam", "null");
			return "my-offers";
		}
		
		List<TransferOffer> allOffers = transferService.getMyOffers(mgrTeam.getTeamId());
		
		if(allOffers == null){
			model.addAttribute("noOffers", "null");
			return "my-offers";
		}
		model.addAttribute("myoffers", allOffers);
		return "my-offers";
	}
	
	@ModelAttribute("accepted-offer")
	public TransferOffer transferOffer() {
		return new TransferOffer();
	}
	
	
	@RequestMapping(value="/transfers/offers", method= RequestMethod.POST)
	public String acceptOffer(@ModelAttribute("accepted-offer") TransferOffer offer, BindingResult result){
		if(!result.hasErrors()){
			if(offer.getAccepted() == 1){
				TransferOffer to = transferService.findOneById(offer.getOfferId());
				Contract old_contract = contractService.findContractByPlayerId(to.getPlayer().getPlayerId());
				contractService.deleteContract(old_contract);
				
				Team seller = old_contract.getTeam();
				seller.setBuget(seller.getBuget()+to.getPrice());
				
				Team buyer = to.getTeam();
				buyer.setBuget(buyer.getBuget() - to.getPrice());
				
				Contract new_contract = new Contract();
				ContractPK cpk = new ContractPK();
				cpk.setPlayerId(to.getPlayer().getPlayerId());
				cpk.setTeamId(to.getTeam().getTeamId());
				
				new_contract.setId(cpk);
				new_contract.setPlayer(to.getPlayer());
				new_contract.setTeam(to.getTeam());
				
				List<TransferOffer> player_offers = transferService.getOnePlayerOffers(to.getPlayer().getPlayerId());
				contractService.save(new_contract);
				
				for(int i = 0 ; i < player_offers.size(); i++)
					transferService.delete(player_offers.get(i));
				teamService.save(seller);
				teamService.save(buyer);
				
			}
		}
		
		return "my-offers";
	}
	
}
