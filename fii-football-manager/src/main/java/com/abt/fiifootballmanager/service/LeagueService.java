package com.abt.fiifootballmanager.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.League;
import com.abt.fiifootballmanager.repository.LeagueRepository;

@Service
@Transactional
public class LeagueService {

	@Autowired
	private LeagueRepository leagueRepository;
	
	public List<League> getAllLeagues(){
		List<League> leagues = new ArrayList<League>();
		leagues = leagueRepository.findAll();
		return leagues;
	}

	public League findOneByName(String league_name) {
		return leagueRepository.findOneByName(league_name);
	}

	public League findOne(Long id) {
		return leagueRepository.findOne(id);
	}
	
	public List<League> findAll(){
		return leagueRepository.findAll();
	}
	
}
