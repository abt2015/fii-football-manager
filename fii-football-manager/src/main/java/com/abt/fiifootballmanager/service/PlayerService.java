package com.abt.fiifootballmanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.repository.PlayerRepository;

@Transactional
@Service
public class PlayerService {
	
	@Autowired
	private PlayerRepository playerRepository;
	
	private static final int PAGE_SIZE = 25;
	
	public int maxId(){
		return playerRepository.findMax()+1;
	}
	
	public void save(Player player){
		playerRepository.save(player);
	}
	public List<Player> findAll() {
		return playerRepository.findAll();
	}

	public Player findByPlayerId(long playerId) {
		return playerRepository.findOne(playerId);
	}
	
    public Page<Player> getPlayers(Integer pageNumber) {
        PageRequest request =
            new PageRequest(pageNumber - 1, PAGE_SIZE, Direction.ASC, "firstName");
        return playerRepository.findAll(request);
    }
    
}
