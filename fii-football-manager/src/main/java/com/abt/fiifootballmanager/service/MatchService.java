package com.abt.fiifootballmanager.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Match;
import com.abt.fiifootballmanager.repository.MatchRepository;

@Service
public class MatchService {
	
	@Autowired
	private MatchRepository matchRepository;
	
	public void saveMatch(Match match){
		matchRepository.save(match);
	}
	
	public Match getMatch(long id){
		return matchRepository.findOne(id);
	}
	
	public void deleteMatch(Match match){
		matchRepository.delete(match);
	}
	
	public void simulateMatch(HttpServletResponse response, Match match){
		match.simulate(response);
	}

}
