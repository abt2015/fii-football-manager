package com.abt.fiifootballmanager.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Stadium;
import com.abt.fiifootballmanager.repository.StadiumRepository;

@Service
@Transactional
public class StadiumService {
	
	@Autowired
	private StadiumRepository stadiumRepository;
	
	public Stadium findById(long stadium_id){
		return stadiumRepository.findOne(stadium_id);
	}
}
