package com.abt.fiifootballmanager.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.abt.fiifootballmanager.repository.ContractRepository;
import com.abt.fiifootballmanager.repository.LeagueRepository;
import com.abt.fiifootballmanager.repository.ManagerRepository;
import com.abt.fiifootballmanager.repository.PlayerRepository;
import com.abt.fiifootballmanager.repository.ProfileRepository;
import com.abt.fiifootballmanager.repository.RoleRepository;
import com.abt.fiifootballmanager.repository.StadiumRepository;
import com.abt.fiifootballmanager.repository.TeamRepository;

@Service
@Transactional
public class InitDbService {
	
	@Autowired
	private ManagerRepository managerRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private LeagueRepository leagueRepository;
	
	@Autowired
	private PlayerRepository playerRepository;
	
	@Autowired 
	private ContractRepository contractRepository;
	
	@Autowired
	private StadiumRepository stadiumRepository;
	
	/*
	@PostConstruct
	private void init(){
		Manager mgr = new Manager();
		mgr.setUsername("gabriel.tifui");
		mgr.setEmail("gabriel.tifui@info.uaic.ro");
		mgr.setPassword("delete00");
		mgr.setPassword("delete00");
		mgr.setActive(1);
		Role adminRole = new Role();
		adminRole.setRole("ADMIN");
		
		Role userRole = new Role();
		userRole.setRole("USER");
		
		ArrayList<Role> roles = new ArrayList<Role>();
		roles.add(adminRole);
		roles.add(userRole);
		
		Profile profile = new Profile();
		profile.setUsername(mgr.getUsername());
		profile.setFirstName("Tifui");
		profile.setSecondName("Gabriel");
		profile.setFavouriteTeam("FC Rapid");
		profileRepository.save(profile);
		roleRepository.save(roles);
		
		mgr.setRoles(roles);
		mgr.setProfile(profile);
		managerRepository.save(mgr);
	}
	*/
	/*
	@PostConstruct
	private void initTeam(){

		Team rapid = new Team();
		Team dinamo = new Team();
		Team real = new Team();
		
		rapid.setTeamName("Rapid");
		rapid.setTeamFans(new BigDecimal(20000));
		
		dinamo.setTeamName("FC Dinamo");
		dinamo.setTeamFans(new BigDecimal(15000));
		
		real.setTeamName("Real Madrid");
		real.setTeamFans(new BigDecimal(50000000));
		
		League liga = new League();
		liga.setName("Liga 1");
		liga.setCountry("Romania");
		liga.setReputation(new BigDecimal(2000));
		liga.setTeamsNo(new BigDecimal(18));
		
		League la_liga = new League();
		la_liga.setName("La Liga");
		la_liga.setCountry("Spain");
		la_liga.setReputation(new BigDecimal(100000));
		la_liga.setTeamsNo(new BigDecimal(20));
		
		rapid.setLeague(liga);
		dinamo.setLeague(liga);
		real.setLeague(la_liga);
		
		leagueRepository.save(liga);
		leagueRepository.save(la_liga);
		teamRepository.save(rapid);
		teamRepository.save(dinamo);
		teamRepository.save(real);
	}
	
	@PostConstruct
	private void initPlayers(){
		Player player1 = new Player();
		player1.setNationality("Romanian");
		player1.setFirstName("Daniel");
		player1.setSecondName("Niculae");
		player1.setDateOfBirth(new Date(20/11/1982));
		player1.setMainPosition("ST");
		player1.setAlias("Nico");
		
		Player player2 = new Player();
		player2.setNationality("Romanian");
		player2.setFirstName("Marius");
		player2.setSecondName("Niculae");
		player2.setDateOfBirth(new Date(18/01/1980));
		player2.setMainPosition("ST");
		player2.setAlias("Cainele");
		
		Player player3 = new Player();
		player3.setNationality("Romanian");
		player3.setFirstName("Cristian");
		player3.setSecondName("Sapunaru");
		player3.setDateOfBirth(new Date(18/01/1980));
		player3.setMainPosition("ST");
		player3.setAlias("Sapun");
		
		playerRepository.save(player1);
		playerRepository.save(player2);
		playerRepository.save(player3);
		
		Contract contract1 = new Contract();
		ContractPK pk1 = new ContractPK();
		pk1.setPlayerId(player1.getPlayerId());
		pk1.setTeamId((long)6);
		contract1.setId(pk1);
		contract1.setPlayer(player1);
		contract1.setSalary(new BigDecimal(10.233));
		contract1.setStartDate(new Date(20/05/2014));
		contract1.setEndDate(new Date(10/05/2015));
		contract1.setTeam(teamRepository.findOne((long) 6));
		
		Contract contract2 = new Contract();
		ContractPK pk2 = new ContractPK();
		pk2.setPlayerId(player2.getPlayerId());
		pk2.setTeamId((long)7);
		contract2.setId(pk2);
		contract2.setPlayer(player2);
		contract2.setSalary(new BigDecimal(5.233));
		contract2.setStartDate(new Date(21/07/2014));
		contract2.setEndDate(new Date(04/11/2016));
		contract2.setTeam(teamRepository.findOne((long) 7));
		
		Contract contract3 = new Contract();
		ContractPK pk3 = new ContractPK();
		pk3.setPlayerId(player1.getPlayerId());
		pk3.setTeamId((long)6);
		contract3.setId(pk3);
		contract3.setPlayer(player3);
		contract3.setSalary(new BigDecimal(25.200));
		contract3.setStartDate(new Date(10/02/2015));
		contract3.setEndDate(new Date(10/10/2016));
		
		contractRepository.save(contract1);
		contractRepository.save(contract2);
		contractRepository.save(contract3);
	}
	
	
	@PostConstruct
	public void initStadiums(){
		Stadium giulesti = new Stadium();
		giulesti.setStadiumName("Valentin Stanescu");
		giulesti.setCapacity(new BigDecimal(19.200));
		giulesti.setBuildDate(new Date(19/23/1923));
		stadiumRepository.save(giulesti);

		long id = 0;
		Team team;
		for(id = 0; id < (long) 100 ; id++){
			team = teamRepository.findOne(id);
			if(team != null){
				team.setStadium(giulesti);
				teamRepository.save(team);
				break;
			}
		}
	}
	*/
}