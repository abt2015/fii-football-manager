package com.abt.fiifootballmanager.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Profile;
import com.abt.fiifootballmanager.repository.ProfileRepository;

@Service
@Transactional
public class ProfileService {
	
	@Autowired
	private ProfileRepository profileRepository;

	public void save(Profile profile) {
		profileRepository.save(profile);
	}

	public Profile findProfileId(long id) {
		Profile profile = profileRepository.findOneWithManagerId(id);
		return profile;
	}
	
	public Profile findProfileByUsername(String username){
		Profile profile = profileRepository.findOneWithUsername(username);
		return profile;
	}

}
