package com.abt.fiifootballmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.entity.TransferOffer;
import com.abt.fiifootballmanager.repository.TransferOfferRepository;


@Service
public class TransferOfferService {
	
	@Autowired
	private TransferOfferRepository transferRepository;
	
	public void save(TransferOffer offer){
		transferRepository.save(offer);
	}
	
	public List<TransferOffer> getOnePlayerOffers(long playerId){
		return transferRepository.findOnePlayerOffers(playerId);
	}
	public List<TransferOffer> findAll(){
		return transferRepository.findAll();
	}
	
	public TransferOffer findOneById(long offerId){
		return transferRepository.findOne(offerId);
	}
	
	public List<TransferOffer> getMyOffers(long teamid){
		return transferRepository.getMyOffers(teamid);
	}
	
	public List<Player> getAllOfferedPlayers(){
		List<TransferOffer> offers = findAll();
		List<Player> players = new ArrayList<Player>();
		for(int i =0 ; i < offers.size(); i++){
			players.add(offers.get(i).getPlayer());
		}
		return players;
	}

	public void delete(TransferOffer to) {
		transferRepository.delete(to);
	}
	
}
