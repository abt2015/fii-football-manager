package com.abt.fiifootballmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Contract;
import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.repository.ContractRepository;

@Service
public class ContractService {
	
	@Autowired
	private ContractRepository contractRepository;
	
	public List<Player> findTeamPlayers(long teamId){
		List<Contract> team_contracts = contractRepository.findOneTeamContracts(teamId);
		List<Player> my_players = new ArrayList<Player>();
		for(int i = 0; i<my_players.size(); i++)
			my_players.add(team_contracts.get(i).getPlayer());
		
		return my_players;
	}
	public List<Contract> findOneTeamContracts(long teamId) {
		List<Contract> contracts = contractRepository.findOneTeamContracts(teamId);
		return contracts;
	}
	
	public void save(Contract contract){
		contractRepository.save(contract);
	}
	
	public List<Contract> findAll(){
		return contractRepository.findAll();
	}
	
	public void deleteContract(Contract old_contract){
		contractRepository.delete(old_contract);
	}
	
	public Contract findContractByPlayerId(long playerId) {
		return contractRepository.findByPlayerId(playerId);
	}
}
