package com.abt.fiifootballmanager.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Manager;
import com.abt.fiifootballmanager.entity.Profile;
import com.abt.fiifootballmanager.entity.Role;
import com.abt.fiifootballmanager.exceptions.EmailExistsException;
import com.abt.fiifootballmanager.repository.ManagerRepository;

@Service
@Transactional
public class ManagerService {
	
	@Autowired
	private ManagerRepository managerRepository;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private ProfileService profileService;
	
	public void save(Manager manager) {
		Role userRole = roleService.findByName("USER");
		List<Role> roles = new ArrayList<Role>();
		roles.add(userRole);
		manager.setRoles(roles);
		manager.setActive(1);
		
		managerRepository.save(manager);
	}

	public Manager findOne(long mgrId) {
		return managerRepository.findOne(mgrId);
	}
	
	public Manager findOne(String username){
		return managerRepository.findOne(username);
	}
	public void registerNewManager(Manager manager) throws EmailExistsException{
		if(emailExist(manager.getEmail())) 
			throw new EmailExistsException("There is an account with this email address: "+manager.getEmail());
		
		Role userRole = roleService.findByName("USER");
		List<Role> roles = new ArrayList<Role>();
		roles.add(userRole);
		manager.setRoles(roles);
		manager.setActive(1);
		
		Profile profile = new Profile();
		profile.setUsername(manager.getUsername());
		profileService.save(profile);
		managerRepository.save(manager);
 	}
	private boolean emailExist(String email){
		Manager mgr = managerRepository.findOneByEmail(email);
		if(mgr != null)
			return true;
		else
			return false;
	}
	
}
