package com.abt.fiifootballmanager.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Mental;
import com.abt.fiifootballmanager.entity.Outfield;
import com.abt.fiifootballmanager.entity.Player;
import com.abt.fiifootballmanager.entity.SkillsEnum;
import com.abt.fiifootballmanager.entity.Team;
import com.abt.fiifootballmanager.repository.OutfieldRepository;
import com.abt.fiifootballmanager.repository.TeamRepository;

@Service
@Transactional
public class TeamService {

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private OutfieldRepository outfieldRepository;
	
	@Autowired
	private PlayerService playerService;

	public List<Team> getFreeTeams() {
		List<Team> teams = new ArrayList<Team>();
		teams = teamRepository.getFreeTeams();
		return teams;
	}

	public Team getOne(long team_id) {
		return teamRepository.findOne(team_id);
	}

	public Team getOneByManager(long manager_id) {
		return teamRepository.findOneByManagerId(manager_id);
	}

	public Team getOne(String teamName) {
		return teamRepository.findOneByName(teamName);
	}

	public void save(Team team) {
		teamRepository.save(team);
	}

	public void trainTeam(Team team) {
		List<Player> team_players = team.getAllPlayers();
		for(int i = 0; i < team_players.size(); i++)
			if(team.getTrain().equals(SkillsEnum.OUTFIELD.toString()))
				trainOutfield(team_players.get(i));
			else if(team.getTrain().equals(SkillsEnum.MENTAL.toString()))
				trainMental(team_players.get(i));
		
		teamRepository.save(team);
	}

	private void trainOutfield(Player player) {
		Outfield p_outfield = new Outfield();
		if (player.getSkill() != null) {
			p_outfield = player.getSkill().getOutfield();
			p_outfield.setCorners(p_outfield.getCorners() + 0.01 * p_outfield.getCorners());
			p_outfield.setCrossing(p_outfield.getCrossing() + 0.01 * p_outfield.getCrossing());
			p_outfield.setDriblling(p_outfield.getDriblling()+ 0.01 * p_outfield.getDriblling());
			p_outfield.setFinishing(p_outfield.getFinishing() + 0.01 * p_outfield.getFinishing());
			p_outfield.setFreeKickTaking(p_outfield.getFreeKickTaking() + 0.01 * p_outfield.getFreeKickTaking());
			p_outfield.setHeading(p_outfield.getHeading() + 0.01 * p_outfield.getHeading());
			p_outfield.setLongShots(p_outfield.getLongShots() + 0.01 * p_outfield.getLongShots());
			p_outfield.setLongThrows(p_outfield.getLongThrows() + 0.01 * p_outfield.getLongThrows());
			p_outfield.setPassing(p_outfield.getPassing() + 0.01 * p_outfield.getPassing());
		}
		outfieldRepository.save(p_outfield);
	}

	private void trainMental(Player player) {
		Mental p_mental = player.getSkill().getMental();
		p_mental.setAggression(p_mental.getAggression() + 0.01 * p_mental.getAggression());
		p_mental.setAnticipation(p_mental.getAnticipation() + 0.01 * p_mental.getAnticipation());
		p_mental.setCreativity(p_mental.getCreativity() + 0.01 * p_mental.getCreativity());
		p_mental.setPositioning(p_mental.getPositioning() + 0.01 * p_mental.getPositioning());
		p_mental.setTeamwork(p_mental.getTeamwork() + 0.01 * p_mental.getTeamwork());
		p_mental.setTeamwork(p_mental.getTeamwork() + 0.01 * p_mental.getTeamwork());
	}
}
