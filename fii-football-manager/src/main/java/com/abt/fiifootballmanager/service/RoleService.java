package com.abt.fiifootballmanager.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abt.fiifootballmanager.entity.Role;
import com.abt.fiifootballmanager.repository.RoleRepository;

@Service
@Transactional
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	public Role findByName(String roleName){
		return roleRepository.findOne(roleName);
	}
}
