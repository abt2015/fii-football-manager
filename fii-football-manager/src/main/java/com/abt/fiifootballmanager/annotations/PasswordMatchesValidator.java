package com.abt.fiifootballmanager.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.abt.fiifootballmanager.entity.Manager;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
	@Override
    public void initialize(PasswordMatches constraintAnnotation) {      
    }
	
	 @Override
	    public boolean isValid(Object obj, ConstraintValidatorContext context){  
	        Manager mgr = (Manager) obj;
	        return mgr.getPassword().equals(mgr.getPassword());   
	    }   
}
