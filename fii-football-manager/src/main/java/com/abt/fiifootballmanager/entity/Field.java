package com.abt.fiifootballmanager.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fiifootballmanager.algorithms.Decision;

public class Field {

	private Player[][] field;
	private List<Player> home_players;
	private List<Player> away_players;
	private final Ball ball = new Ball();

	public Field() {
		field = new Player[200][200];
		home_players = new ArrayList<Player>();
		away_players = new ArrayList<Player>();
	}

	public void simulate(HttpServletResponse response) {
		double start_time = System.currentTimeMillis() / 60000;
		while (System.currentTimeMillis() / 60000 < start_time + 5) {
			Player player = field[ball.getI()][ball.getJ()];
			Decision decision = player.getAttackDecision();
			if (decision != null && player != null)
				action(decision, player, response);
		}
	}

	private void action(final Decision decision, Player player,
			HttpServletResponse response) {
		if (decision.equals(Decision.PASS)) {
			ball.setI(getNearestTeammate(player).getI());
			ball.setJ(getNearestTeammate(player).getJ());
			try {
				response.getWriter().println("A pasat");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (decision.equals(Decision.SHOT)) {
			Player gk = new Player();
			if (home_players.contains(player))
				gk = field[25][100];
			else
				gk = field[25][1];
			double distance = distance(player.getI(), player.getJ(), gk.getI(), gk.getJ());
			if (distance > 20) {
				if (player.getSkill().getOutfield().getLongShots() > gk
						.getSkill().getGoalkeeping().getPunching())
					try {
						response.getWriter().println("GOOOOOOOOOL");
					} catch (IOException e) {
						e.printStackTrace();
					}
				ball.setI(25);
				ball.setJ(49);
			}
		}
	}

	private Player getNearestTeammate(Player player) {
		Team player_team = player.getContracts().get(0).getTeam();
		List<Player> teammates = player_team.getPlayers();
		teammates.remove(player);
		double min_dist = 0;
		Player closest_teammate = new Player();
		for (int i = 0; i < teammates.size(); i++) {
			Player mate = teammates.get(i);
			if (distance(player.getI(), player.getJ(), mate.getI(), mate.getJ()) < min_dist) {
				min_dist = distance(player.getI(), player.getJ(), mate.getI(),
						mate.getJ());
				closest_teammate = mate;
			}
		}
		return closest_teammate;
	}

	private double distance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow((double) (x1 - x2), 2.0)
				+ Math.pow((double) (y1 - y2), 2.0));
	}

	public void setPlayers(Team home_team, Team away_team) {
		this.home_players = home_team.getPlayers();
		this.away_players = away_team.getPlayers();
		List<Player> home_players = home_team.getPlayers();
		List<Player> away_players = away_team.getPlayers();

		arrangePlayers(1, 1, 50, home_players);
		arrangePlayers(100, 51, 100, away_players);

	}

	public Ball getBall() {
		return this.ball;
	}

	private void arrangePlayers(int gk_col, int min, int max, List<Player> team) {
		boolean ball = false;
		for (int i = 0; i < team.size(); ++i)
			if (ball == false && team.get(i).getTeamPosition().equals("ST")) {
				field[25][49] = team.get(i);
				team.get(i).setI(25);
				team.get(i).setI(49);
				this.ball.setI(25);
				this.ball.setJ(49);
			} else if (team.get(i).getTeamPosition().equals("GK")) {
				field[25][gk_col] = team.get(i);
				team.get(i).setI(1);
				team.get(i).setJ(gk_col);
			} else {
				int l = 1 + (int) (Math.random() * 50);
				int c = min + (int) (Math.random() * max);

				if (field[l][c] != null) {
					field[l][c] = team.get(i);
					team.get(i).setI(l);
					team.get(i).setJ(c);
				} else
					while (field[l][c] != null) {
						l = 1 + (int) (Math.random() * 50);
						c = min + (int) (Math.random() * max);
						field[l][c] = team.get(i);
						team.get(i).setI(l);
						team.get(i).setJ(c);
					}
			}
	}

}
