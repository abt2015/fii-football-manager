package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the OUTFIELD database table.
 * 
 */
@Entity
@NamedQuery(name="Outfield.findAll", query="SELECT o FROM Outfield o")
public class Outfield implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PLAYER_ID")
	private long playerId;

	private double corners;

	private double crossing;

	private double driblling;

	private double finishing;

	@Column(name="FREE_KICK_TAKING")
	private double freeKickTaking;

	private double heading;

	@Column(name="LONG_SHOTS")
	private double longShots;

	@Column(name="LONG_THROWS")
	private double longThrows;

	private double marking;

	private double passing;

	@Column(name="PENALTY_TAKING")
	private double penaltyTaking;

	private double tackling;

	//bi-directional one-to-one association to Skill
	@OneToOne
	@JoinColumn(name="PLAYER_ID")
	private Skill skill;
	
	public List<SkillValue> getAttackSkills(){
		List<SkillValue> list = new ArrayList<SkillValue>();
		list.add(new SkillValue("passing", passing));
		list.add(new SkillValue("finishing", finishing));
		list.add(new SkillValue("crossing", crossing));
		list.add(new SkillValue("longshots", longShots));
		list.add(new SkillValue("driblling", driblling));
		list.add(new SkillValue("finishing", finishing));
		
		return list;
	}
	
	public List<SkillValue> getDefenseSkills(){
		List<SkillValue> list = new ArrayList<SkillValue>();
		list.add(new SkillValue("tackling", tackling));
		return list;
	}
	
	public double getSum(){
		return this.corners + this.crossing + this.driblling + this.finishing + this.freeKickTaking + 
				this.heading + this.longShots + this.longThrows + this.marking + this.passing + 
				this.penaltyTaking + this.tackling;
	}
	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public double getCorners() {
		return this.corners;
	}

	public void setCorners(double corners) {
		this.corners = corners;
	}

	public double getCrossing() {
		return this.crossing;
	}

	public void setCrossing(double crossing) {
		this.crossing = crossing;
	}

	public double getDriblling() {
		return this.driblling;
	}

	public void setDriblling(double driblling) {
		this.driblling = driblling;
	}

	public double getFinishing() {
		return this.finishing;
	}

	public void setFinishing(double finishing) {
		this.finishing = finishing;
	}

	public double getFreeKickTaking() {
		return this.freeKickTaking;
	}

	public void setFreeKickTaking(double freeKickTaking) {
		this.freeKickTaking = freeKickTaking;
	}

	public double getHeading() {
		return this.heading;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public double getLongShots() {
		return this.longShots;
	}

	public void setLongShots(double longShots) {
		this.longShots = longShots;
	}

	public double getLongThrows() {
		return this.longThrows;
	}

	public void setLongThrows(double longThrows) {
		this.longThrows = longThrows;
	}

	public double getMarking() {
		return this.marking;
	}

	public void setMarking(double marking) {
		this.marking = marking;
	}

	public double getPassing() {
		return this.passing;
	}

	public void setPassing(double passing) {
		this.passing = passing;
	}

	public double getPenaltyTaking() {
		return this.penaltyTaking;
	}

	public void setPenaltyTaking(double penaltyTaking) {
		this.penaltyTaking = penaltyTaking;
	}

	public double getTackling() {
		return this.tackling;
	}

	public void setTackling(double tackling) {
		this.tackling = tackling;
	}

	public Skill getSkill() {
		return this.skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

}