package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the STADIUM database table.
 * 
 */
@Entity
@NamedQuery(name="Stadium.findAll", query="SELECT s FROM Stadium s")
public class Stadium implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STADIUM_ID")
	private long stadiumId;

	@Temporal(TemporalType.DATE)
	@Column(name="BUILD_DATE")
	private Date buildDate;

	private BigDecimal capacity;
	
	private String stadiumName;

	//bi-directional one-to-one association to Location
	@OneToOne(mappedBy="stadium")
	private Location location;

	//bi-directional many-to-one association to Team
	@OneToMany(mappedBy="stadium")
	private List<Team> teams;

	public Stadium() {
	}

	public long getStadiumId() {
		return this.stadiumId;
	}

	public void setStadiumId(long stadiumId) {
		this.stadiumId = stadiumId;
	}

	public Date getBuildDate() {
		return this.buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public BigDecimal getCapacity() {
		return this.capacity;
	}

	public void setCapacity(BigDecimal capacity) {
		this.capacity = capacity;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Team> getTeams() {
		return this.teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Team addTeam(Team team) {
		getTeams().add(team);
		team.setStadium(this);

		return team;
	}

	public Team removeTeam(Team team) {
		getTeams().remove(team);
		team.setStadium(null);

		return team;
	}
	
	public String getStadiumName() {
		return stadiumName;
	}

	public void setStadiumName(String stadiumName) {
		this.stadiumName = stadiumName;
	}
}