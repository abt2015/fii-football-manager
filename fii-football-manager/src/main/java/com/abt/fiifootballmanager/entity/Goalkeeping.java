package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GOALKEEPING database table.
 * 
 */
@Entity
@NamedQuery(name="Goalkeeping.findAll", query="SELECT g FROM Goalkeeping g")
public class Goalkeeping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PLAYER_ID")
	private long playerId;

	@Column(name="AERIAL_ABILITY")
	private double aerialAbility;

	private double communication;

	@Column(name="FREE_KICK_KEEPING")
	private double freeKickKeeping;

	@Column(name="ONE_ON_ONES")
	private double oneOnOnes;

	@Column(name="PENALTY_KEEPING")
	private double penaltyKeeping;

	private double punching;

	private double reflexes;

	//bi-directional one-to-one association to Skill
	@OneToOne
	@JoinColumn(name="PLAYER_ID")
	private Skill skill;

	public Goalkeeping() {
	}

	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public double getAerialAbility() {
		return this.aerialAbility;
	}

	public void setAerialAbility(double aerialAbility) {
		this.aerialAbility = aerialAbility;
	}

	public double getCommunication() {
		return this.communication;
	}

	public void setCommunication(double communication) {
		this.communication = communication;
	}

	public double getFreeKickKeeping() {
		return this.freeKickKeeping;
	}

	public void setFreeKickKeeping(double freeKickKeeping) {
		this.freeKickKeeping = freeKickKeeping;
	}

	public double getOneOnOnes() {
		return this.oneOnOnes;
	}

	public void setOneOnOnes(double oneOnOnes) {
		this.oneOnOnes = oneOnOnes;
	}

	public double getPenaltyKeeping() {
		return this.penaltyKeeping;
	}

	public void setPenaltyKeeping(double penaltyKeeping) {
		this.penaltyKeeping = penaltyKeeping;
	}

	public double getPunching() {
		return this.punching;
	}

	public void setPunching(double punching) {
		this.punching = punching;
	}

	public double getReflexes() {
		return this.reflexes;
	}

	public void setReflexes(double reflexes) {
		this.reflexes = reflexes;
	}

	public Skill getSkill() {
		return this.skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

}