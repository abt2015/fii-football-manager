package com.abt.fiifootballmanager.entity;

public class Ball {
	
	private final String name = "ball";
	
	private int i, j;
	
	public String getName() {
		return name;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}
	
}
