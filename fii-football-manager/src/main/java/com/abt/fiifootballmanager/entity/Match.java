package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.*;
import javax.servlet.http.HttpServletResponse;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="\"MATCH\"")
@NamedQuery(name="Match.findAll", query="SELECT m FROM Match m")
public class Match implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="MATCH_ID")
	private long matchId;
	private BigDecimal attendance;

	@Temporal(TemporalType.DATE)
	@Column(name="MATCH_DATE")
	private Date matchDate;

	@Column(name="TICKET_PRICE")
	private BigDecimal ticketPrice;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="TEAM_TEAM_ID")
	private Team team1;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="TEAM_TEAM_ID1")
	private Team team2;

	//bi-directional many-to-one association to Performance
	@OneToMany(mappedBy="match", cascade={CascadeType.REFRESH})
	private List<Performance> performances;
	
	@Column(name="ROUND")
	private int round;
	
	@Column(name="CHALLENGE")
	private int challenge;
	
	@Transient
	private Field field;
	
	public void simulate(HttpServletResponse response){
		field = new Field();
		field.setPlayers(this.team1, this.team2);
		field.simulate(response);
	}
	
	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public long getMatchId() {
		return this.matchId;
	}

	public void setMatchId(long matchId) {
		this.matchId = matchId;
	}

	public BigDecimal getAttendance() {
		return this.attendance;
	}

	public void setAttendance(BigDecimal attendance) {
		this.attendance = attendance;
	}

	public Date getMatchDate() {
		return this.matchDate;
	}

	public void setMatchDate(Date matchDate) {
		this.matchDate = matchDate;
	}

	public BigDecimal getTicketPrice() {
		return this.ticketPrice;
	}

	public void setTicketPrice(BigDecimal ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	public Team getTeam1() {
		return this.team1;
	}

	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	public Team getTeam2() {
		return this.team2;
	}

	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

	public List<Performance> getPerformances() {
		return this.performances;
	}

	public void setPerformances(List<Performance> performances) {
		this.performances = performances;
	}

	public Performance addPerformance(Performance performance) {
		getPerformances().add(performance);
		performance.setMatch(this);

		return performance;
	}

	public Performance removePerformance(Performance performance) {
		getPerformances().remove(performance);
		performance.setMatch(null);

		return performance;
	}

	public int getChallenge() {
		return challenge;
	}

	public void setChallenge(int challenge) {
		this.challenge = challenge;
	}
	
	
}