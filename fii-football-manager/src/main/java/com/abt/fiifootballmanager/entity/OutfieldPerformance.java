package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="OUTFIELD_PERFORMANCES")
@NamedQuery(name="OutfieldPerformance.findAll", query="SELECT o FROM OutfieldPerformance o")
public class OutfieldPerformance implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OutfieldPerformancePK id;

	@Column(name="BAD_CROSSES")
	private int badCrosses;

	@Column(name="BAD_DRIBBLINGS")
	private int badDribblings;

	@Column(name="BAD_PENALTY")
	private int badPenalty;

	@Column(name="BAD_TACKLES")
	private int badTackles;

	private int goals;

	@Column(name="GOOD_CROSSES")
	private int goodCrosses;

	@Column(name="GOOD_DRIBBLINGS")
	private int goodDribblings;

	@Column(name="GOOD_PENALTY")
	private int goodPenalty;

	@Column(name="GOOD_TACKLES")
	private int goodTackles;

	@Column(name="LONG_SHOTS_OFF_TARGET")
	private int longShotsOffTarget;

	@Column(name="LONG_SHOTS_ON_TARGET")
	private int longShotsOnTarget;

	@Column(name="MISSED_GOALS")
	private int missedGoals;

	private int rating;

	//bi-directional many-to-one association to Performance
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="MATCH_ID1", referencedColumnName="MATCH_ID",insertable = false, updatable = false),
		@JoinColumn(name="PLAYER_ID1", referencedColumnName="PLAYER_ID",insertable = false, updatable = false)
		})
	private Performance performance;

	public OutfieldPerformance() {
	}

	public OutfieldPerformancePK getId() {
		return this.id;
	}

	public void setId(OutfieldPerformancePK id) {
		this.id = id;
	}

	public int getBadCrosses() {
		return this.badCrosses;
	}

	public void setBadCrosses(int badCrosses) {
		this.badCrosses = badCrosses;
	}

	public int getBadDribblings() {
		return this.badDribblings;
	}

	public void setBadDribblings(int badDribblings) {
		this.badDribblings = badDribblings;
	}

	public int getBadPenalty() {
		return this.badPenalty;
	}

	public void setBadPenalty(int badPenalty) {
		this.badPenalty = badPenalty;
	}

	public int getBadTackles() {
		return this.badTackles;
	}

	public void setBadTackles(int badTackles) {
		this.badTackles = badTackles;
	}

	public int getGoals() {
		return this.goals;
	}

	public void setGoals(int goals) {
		this.goals = goals;
	}

	public int getGoodCrosses() {
		return this.goodCrosses;
	}

	public void setGoodCrosses(int goodCrosses) {
		this.goodCrosses = goodCrosses;
	}

	public int getGoodDribblings() {
		return this.goodDribblings;
	}

	public void setGoodDribblings(int goodDribblings) {
		this.goodDribblings = goodDribblings;
	}

	public int getGoodPenalty() {
		return this.goodPenalty;
	}

	public void setGoodPenalty(int goodPenalty) {
		this.goodPenalty = goodPenalty;
	}

	public int getGoodTackles() {
		return this.goodTackles;
	}

	public void setGoodTackles(int goodTackles) {
		this.goodTackles = goodTackles;
	}

	public int getLongShotsOffTarget() {
		return this.longShotsOffTarget;
	}

	public void setLongShotsOffTarget(int longShotsOffTarget) {
		this.longShotsOffTarget = longShotsOffTarget;
	}

	public int getLongShotsOnTarget() {
		return this.longShotsOnTarget;
	}

	public void setLongShotsOnTarget(int longShotsOnTarget) {
		this.longShotsOnTarget = longShotsOnTarget;
	}

	public int getMissedGoals() {
		return this.missedGoals;
	}

	public void setMissedGoals(int missedGoals) {
		this.missedGoals = missedGoals;
	}

	public int getRating() {
		return this.rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Performance getPerformance() {
		return this.performance;
	}

	public void setPerformance(Performance performance) {
		this.performance = performance;
	}

}