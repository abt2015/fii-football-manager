package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fiifootballmanager.algorithms.Decision;

@Entity
@NamedQuery(name="Player.findAll", query="SELECT p FROM Player p")
public class Player implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PLAYER_ID")
	private long playerId;

	@Column(name="\"ALIAS\"")
	private String alias;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="MAIN_POSITION")
	private String mainPosition;
	
	@Column(name="TEAM_POSITION")
	private String teamPosition;
	
	private String nationality;

	@Column(name="SECOND_NAME")
	private String secondName;

	//bi-directional many-to-one association to Contract
	@OneToMany(mappedBy="player")
	private List<Contract> contracts;

	//bi-directional many-to-one association to Performance
	@OneToMany(mappedBy="player")
	private List<Performance> performances;

	//bi-directional one-to-one association to Skill
	@OneToOne(mappedBy="player")
	private Skill skill;
	
	@Transient
	private int age;

	@Transient 
	private int i;
	
	@Transient
	private int j;
	
	
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMainPosition() {
		return this.mainPosition;
	}

	public void setMainPosition(String mainPosition) {
		this.mainPosition = mainPosition;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSecondName() {
		return this.secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public List<Contract> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Contract> contracts) {
		this.contracts = contracts;
	}

	public Contract addContract(Contract contract) {
		getContracts().add(contract);
		contract.setPlayer(this);

		return contract;
	}

	public Contract removeContract(Contract contract) {
		getContracts().remove(contract);
		contract.setPlayer(null);

		return contract;
	}

	public List<Performance> getPerformances() {
		return this.performances;
	}

	public void setPerformances(List<Performance> performances) {
		this.performances = performances;
	}

	public Performance addPerformance(Performance performance) {
		getPerformances().add(performance);
		performance.setPlayer(this);

		return performance;
	}

	public Performance removePerformance(Performance performance) {
		getPerformances().remove(performance);
		performance.setPlayer(null);

		return performance;
	}

	public Skill getSkill() {
		return this.skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public String getTeamPosition() {
		return teamPosition;
	}

	public void setTeamPosition(String teamPosition) {
		this.teamPosition = teamPosition;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public Decision getDefenseDecision(){
		return Decision.TACKLE;
	}
	
	public Decision getAttackDecision(){
		List<SkillValue> skills = this.skill.getOutfield().getAttackSkills();
		SkillValue main_skill = new SkillValue("null",0);
		for(int i = 0; i<skills.size(); i++)
			if(skills.get(i).getValue() > main_skill.getValue()){
				main_skill.setName(skills.get(i).getName());
				main_skill.setValue(skills.get(i).getValue());
			}
		
		if(main_skill.getName().equals("passing"))
			return Decision.PASS;
		if(main_skill.getName().equals("finishing"))
			return Decision.SHOT;
		if(main_skill.getName().equals("driblling"))
			return Decision.DRIBBLE;
		if(main_skill.getName().equals("longshots"))
			return Decision.SHOT;
		if(main_skill.getName().equals("crossing"))
			return Decision.CROSS;
		
		return Decision.PASS;
	}
	
}