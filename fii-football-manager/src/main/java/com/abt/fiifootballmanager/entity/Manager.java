package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.abt.fiifootballmanager.annotations.PasswordMatches;
import com.abt.fiifootballmanager.annotations.ValidEmail;
import com.abt.fiifootballmanager.enums.SocialMediaService;

import java.util.List;


@Entity
@Table(name="manager")
@PasswordMatches
public class Manager implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="managerid")
	private long managerId;
	
	@NotEmpty
	@ValidEmail
	private String email;
	
	@NotEmpty
	@NotNull
	private String password;
	
	@Transient
	private String matchingPassword;
	
	@NotEmpty
	@NotNull
	private String username;
	
	private int active;
	
	//bi-directional many-to-one association to Achievement
	@OneToMany(mappedBy="manager")
	private List<Achievement> achievements;

	//bi-directional one-to-one association to Profile
	@OneToOne(mappedBy="manager")
	private Profile profile;

	//bi-directional many-to-one association to Team
	@OneToOne(mappedBy="manager")
	private Team team;

	@ManyToMany
	@JoinTable
	private List<Role> roles;
	
	private SocialMediaService socialSignInProvider;
	
	public long getManagerId() {
		return this.managerId;
	}

	public void setManagerId(long managerId) {
		this.managerId = managerId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public SocialMediaService getSocialSignInProvider() {
		return socialSignInProvider;
	}

	public void setSocialSignInProvider(SocialMediaService socialSignInProvider) {
		this.socialSignInProvider = socialSignInProvider;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Achievement> getAchievements() {
		return this.achievements;
	}

	public void setAchievements(List<Achievement> achievements) {
		this.achievements = achievements;
	}

	public Achievement addAchievement(Achievement achievement) {
		getAchievements().add(achievement);
		achievement.setManager(this);

		return achievement;
	}

	public Achievement removeAchievement(Achievement achievement) {
		getAchievements().remove(achievement);
		achievement.setManager(null);

		return achievement;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}
}