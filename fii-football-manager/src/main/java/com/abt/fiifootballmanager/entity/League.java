package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.List;

@Entity
@NamedQuery(name="League.findAll", query="SELECT l FROM League l")
public class League implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="LEAGUE_ID")
	private long leagueId;
	
	private String name;

	private BigDecimal reputation;

	@Column(name="TEAMS_NO")
	private BigDecimal teamsNo;

	
	@OneToMany(mappedBy="league")
	private List<Achievement> achievements;

	@OneToMany(mappedBy="league")
	List<Team> teams;
	
	private String country;
	
	public long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(long leagueId) {
		this.leagueId = leagueId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getReputation() {
		return this.reputation;
	}

	public void setReputation(BigDecimal reputation) {
		this.reputation = reputation;
	}

	public BigDecimal getTeamsNo() {
		return this.teamsNo;
	}

	public void setTeamsNo(BigDecimal teamsNo) {
		this.teamsNo = teamsNo;
	}

	public List<Achievement> getAchievements() {
		return this.achievements;
	}

	public void setAchievements(List<Achievement> achievements) {
		this.achievements = achievements;
	}

	public Achievement addAchievement(Achievement achievement) {
		getAchievements().add(achievement);
		achievement.setLeague(this);

		return achievement;
	}

	public Achievement removeAchievement(Achievement achievement) {
		getAchievements().remove(achievement);
		achievement.setLeague(null);

		return achievement;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
	
}