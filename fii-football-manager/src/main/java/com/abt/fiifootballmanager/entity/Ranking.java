package com.abt.fiifootballmanager.entity;

import java.util.ArrayList;
import java.util.List;

public class Ranking {

	private League league;
	private List<RankingLine> rows;
	
	public Ranking(League league) {
		this.league = league;
		rows = new ArrayList<RankingLine>();

		List<Team> teams = league.getTeams();

		for (int i = 0; i < teams.size(); i++) {
			String team_name = teams.get(i).getTeamName();
			int points = getHomePoints(teams.get(i));
			RankingLine line = new RankingLine();
			line.setTeamName(team_name);
			line.setTeamPoints(points);
			rows.add(line);
		}
	}

	public void setLeague(League league) {
		this.league = league;
	}

	public League getLeague() {
		return this.league;
	}
	
	public List<RankingLine> getRows(){
		return this.rows;
	}
	private List<Player> getTeamPlayers(Team team) {
		List<Player> players = new ArrayList<Player>();
		for (int i = 0; i < team.getContracts().size(); i++)
			players.add(team.getContracts().get(i).getPlayer());
		return players;
	}

	private int getHomePoints(Team team) {
		List<Match> home_matches = team.getMatches1();
		List<Player> home_players = getTeamPlayers(team);

		int home_team_points = 0;
		int home_team_goals = 0;
		int away_team_goals = 0;
		for (int i = 0; i < home_matches.size(); i++) {
			List<Performance> performances = home_matches.get(i)
					.getPerformances();
			Player player_i = performances.get(i).getPlayer();
			for (int j = 0; j < performances.size(); j++) {
				List<OutfieldPerformance> out_per = performances.get(j)
						.getOutfieldPerformances();
				for (int k = 0; k < out_per.size(); i++) {
					OutfieldPerformance op = out_per.get(i);
					if (op.getGoals() > 0) {
						if (home_players.contains(player_i))
							home_team_goals++;
						else
							away_team_goals++;
					}
				}

			}
			if (home_team_goals > away_team_goals)
				home_team_points +=3;
			else if (home_team_goals == away_team_goals)
				home_team_points +=1;
		}
		
		return home_team_points;
	}
}
