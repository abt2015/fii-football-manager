package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CONTRACT database table.
 * 
 */
@Embeddable
public class ContractPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="TEAM_ID", insertable=false, updatable=false)
	private long teamId;

	@Column(name="PLAYER_ID", insertable=false, updatable=false)
	private long playerId;

	public ContractPK() {
	}
	public long getTeamId() {
		return this.teamId;
	}
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	public long getPlayerId() {
		return this.playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ContractPK)) {
			return false;
		}
		ContractPK castOther = (ContractPK)other;
		return 
			(this.teamId == castOther.teamId)
			&& (this.playerId == castOther.playerId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.teamId ^ (this.teamId >>> 32)));
		hash = hash * prime + ((int) (this.playerId ^ (this.playerId >>> 32)));
		
		return hash;
	}
}