package com.abt.fiifootballmanager.entity;

public class SkillValue {

	private String name;
	private double value;

	public SkillValue(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	public SkillValue(){
		
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

}
