package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the LOCATION database table.
 * 
 */
@Entity
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STADIUM_ID")
	private long stadiumId;

	private String city;

	private String country;

	private String departament;

	//bi-directional one-to-one association to Stadium
	@OneToOne
	@JoinColumn(name="STADIUM_ID")
	private Stadium stadium;

	public Location() {
	}

	public long getStadiumId() {
		return this.stadiumId;
	}

	public void setStadiumId(long stadiumId) {
		this.stadiumId = stadiumId;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDepartament() {
		return this.departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public Stadium getStadium() {
		return this.stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

}