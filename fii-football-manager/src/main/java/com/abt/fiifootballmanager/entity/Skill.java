package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the SKILLS database table.
 * 
 */
@Entity
@Table(name="SKILLS")
@NamedQuery(name="Skill.findAll", query="SELECT s FROM Skill s")
public class Skill implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PLAYER_ID")
	private long playerId;

	private BigDecimal condition;

	@Column(name="FOOT_LEFT")
	private BigDecimal footLeft;

	@Column(name="FOOT_RIGHT")
	private BigDecimal footRight;

	private BigDecimal morale;

	//bi-directional one-to-one association to Goalkeeping
	@OneToOne(mappedBy="skill")
	private Goalkeeping goalkeeping;

	//bi-directional one-to-one association to Mental
	@OneToOne(mappedBy="skill")
	private Mental mental;

	//bi-directional one-to-one association to Outfield
	@OneToOne(mappedBy="skill")
	private Outfield outfield;

	//bi-directional one-to-one association to Physical
	@OneToOne(mappedBy="skill")
	private Physical physical;

	//bi-directional one-to-one association to Player
	@OneToOne
	@JoinColumn(name="PLAYER_ID")
	private Player player;

	public Skill() {
	}

	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public BigDecimal getCondition() {
		return this.condition;
	}

	public void setCondition(BigDecimal condition) {
		this.condition = condition;
	}

	public BigDecimal getFootLeft() {
		return this.footLeft;
	}

	public void setFootLeft(BigDecimal footLeft) {
		this.footLeft = footLeft;
	}

	public BigDecimal getFootRight() {
		return this.footRight;
	}

	public void setFootRight(BigDecimal footRight) {
		this.footRight = footRight;
	}

	public BigDecimal getMorale() {
		return this.morale;
	}

	public void setMorale(BigDecimal morale) {
		this.morale = morale;
	}

	public Goalkeeping getGoalkeeping() {
		return this.goalkeeping;
	}

	public void setGoalkeeping(Goalkeeping goalkeeping) {
		this.goalkeeping = goalkeeping;
	}

	public Mental getMental() {
		return this.mental;
	}

	public void setMental(Mental mental) {
		this.mental = mental;
	}

	public Outfield getOutfield() {
		return this.outfield;
	}

	public void setOutfield(Outfield outfield) {
		this.outfield = outfield;
	}

	public Physical getPhysical() {
		return this.physical;
	}

	public void setPhysical(Physical physical) {
		this.physical = physical;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}