package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the PHYSICAL database table.
 * 
 */
@Entity
@NamedQuery(name="Physical.findAll", query="SELECT p FROM Physical p")
public class Physical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PLAYER_ID")
	private long playerId;

	private double acceleration;

	private double agility;

	private double jumping;

	private double stamina;

	private double strength;

	//bi-directional one-to-one association to Skill
	@OneToOne
	@JoinColumn(name="PLAYER_ID")
	private Skill skill;

	public double getSum(){
		return this.acceleration + this.agility + this.jumping + this.stamina + this.strength;
	}
	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public double getAcceleration() {
		return this.acceleration;
	}

	public void setAcceleration(double acceleration) {
		this.acceleration = acceleration;
	}

	public double getAgility() {
		return this.agility;
	}

	public void setAgility(double agility) {
		this.agility = agility;
	}

	public double getJumping() {
		return this.jumping;
	}

	public void setJumping(double jumping) {
		this.jumping = jumping;
	}

	public double getStamina() {
		return this.stamina;
	}

	public void setStamina(double stamina) {
		this.stamina = stamina;
	}

	public double getStrength() {
		return this.strength;
	}

	public void setStrength(double strength) {
		this.strength = strength;
	}

	public Skill getSkill() {
		return this.skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

}