package com.abt.fiifootballmanager.entity;

import java.util.ArrayList;
import java.util.List;

public class Formation {

	enum Posts {
		GK, LB, CB, RB, LWB, CDM, RWB, LM, CM, RM, LAM, CAM1, CAM2, RAM, LF, CF, RF, ST;
	}

	private Tactic tactic;

	public void setTactic(Tactic tactic) {
		this.tactic = tactic;
	}
	
	public Tactic getTactic(){
		return this.tactic;
	}

	public List<String> getPositions() {
		List<String> posts = new ArrayList<String>();
		if (tactic.equals(Tactic.T1)) {
			posts.add(Posts.GK.toString());
			posts.add(Posts.LB.toString());
			posts.add(Posts.CB.toString() + "1");
			posts.add(Posts.CB.toString() + "2");
			posts.add(Posts.RB.toString());
			posts.add(Posts.LM.toString());
			posts.add(Posts.CM.toString() + "1");
			posts.add(Posts.CM.toString() + "2");
			posts.add(Posts.RM.toString());
			posts.add(Posts.CF.toString() + "1");
			posts.add(Posts.CF.toString() + "2");
		}
		
		if (tactic.equals(Tactic.T3)) {
			posts.add(Posts.GK.toString());
			posts.add(Posts.LB.toString());
			posts.add(Posts.CB.toString() + "1");
			posts.add(Posts.CB.toString() + "2");
			posts.add(Posts.RB.toString());
			posts.add(Posts.LM.toString());
			posts.add(Posts.CM.toString() + "1");
			posts.add(Posts.CM.toString() + "2");
			posts.add(Posts.RM.toString());
			posts.add(Posts.CAM1.toString());
			posts.add(Posts.CF.toString() + "1");
		}
		
		return posts;
	}
}
