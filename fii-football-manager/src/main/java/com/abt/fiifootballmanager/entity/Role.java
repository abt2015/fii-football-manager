package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


@Entity
@Table(name="role")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long roleId;

	private String role;
	
	@ManyToMany(mappedBy = "roles")
	private List<Manager> managers;

	public long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}