package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ACHIEVEMENTS database table.
 * 
 */
@Embeddable
public class AchievementPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="MANAGER_ID", insertable=false, updatable=false)
	private long managerId;

	@Column(name="TEAM_ID", insertable=false, updatable=false)
	private long teamId;

	@Column(name="LEAGUE_ID", insertable=false, updatable=false)
	private long leagueId;

	public AchievementPK() {
	}
	public long getManagerId() {
		return this.managerId;
	}
	public void setManagerId(long managerId) {
		this.managerId = managerId;
	}
	public long getTeamId() {
		return this.teamId;
	}
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	public long getLeagueId() {
		return this.leagueId;
	}
	public void setLeagueId(long leagueId) {
		this.leagueId = leagueId;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AchievementPK)) {
			return false;
		}
		AchievementPK castOther = (AchievementPK)other;
		return 
			(this.managerId == castOther.managerId)
			&& (this.teamId == castOther.teamId)
			&& (this.leagueId == castOther.leagueId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.managerId ^ (this.managerId >>> 32)));
		hash = hash * prime + ((int) (this.teamId ^ (this.teamId >>> 32)));
		hash = hash * prime + ((int) (this.leagueId ^ (this.leagueId >>> 32)));
		
		return hash;
	}
}