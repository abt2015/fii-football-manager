package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name = "Team.findAll", query = "SELECT t FROM Team t")
@NamedStoredProcedureQuery(name = "insertTeams", procedureName = "insertTeams")
public class Team implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TEAM_ID")
	private long teamId;

	@Column(name = "TEAM_FANS")
	private BigDecimal teamFans;

	@Column(name = "TEAM_NAME")
	private String teamName;

	// bi-directional many-to-one association to Achievement
	@OneToMany(mappedBy = "team")
	private List<Achievement> achievements;

	// bi-directional many-to-one association to Contract
	@OneToMany(mappedBy = "team")
	private List<Contract> contracts;

	// bi-directional many-to-one association to League
	@ManyToOne
	@JoinColumn(name = "LEAGUE_ID")
	private League league;

	// bi-directional many-to-one association to Match
	@OneToMany(mappedBy = "team1")
	private List<Match> matches1;

	// bi-directional many-to-one association to Match
	@OneToMany(mappedBy = "team2")
	private List<Match> matches2;

	// bi-directional many-to-one association to Manager
	@OneToOne
	@JoinColumn(name = "MANAGER_ID")
	private Manager manager;

	// bi-directional many-to-one association to Stadium
	@ManyToOne
	@JoinColumn(name = "STADIUM_ID")
	private Stadium stadium;

	@Column(name = "Buget", nullable = false)
	private double buget;

	@Column(name = "Tactic")
	private String tactic;

	@Column(name = "Train")
	private String train;

	public String getTrain() {
		return train;
	}

	public void setTrain(String train) {
		this.train = train;
	}

	public String getTactic() {
		return tactic;
	}

	public void setTactic(String tactic) {
		this.tactic = tactic;
	}

	public double getBuget() {
		return buget;
	}

	public void setBuget(double buget) {
		this.buget = buget;
	}

	public long getTeamId() {
		return this.teamId;
	}

	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}

	public BigDecimal getTeamFans() {
		return this.teamFans;
	}

	public void setTeamFans(BigDecimal teamFans) {
		this.teamFans = teamFans;
	}

	public String getTeamName() {
		return this.teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<Achievement> getAchievements() {
		return this.achievements;
	}

	public void setAchievements(List<Achievement> achievements) {
		this.achievements = achievements;
	}

	public Achievement addAchievement(Achievement achievement) {
		getAchievements().add(achievement);
		achievement.setTeam(this);

		return achievement;
	}

	public Achievement removeAchievement(Achievement achievement) {
		getAchievements().remove(achievement);
		achievement.setTeam(null);

		return achievement;
	}

	public List<Contract> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<Contract> contracts) {
		this.contracts = contracts;
	}

	public Contract addContract(Contract contract) {
		getContracts().add(contract);
		contract.setTeam(this);

		return contract;
	}

	public Contract removeContract(Contract contract) {
		getContracts().remove(contract);
		contract.setTeam(null);

		return contract;
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}

	public List<Match> getMatches1() {
		return this.matches1;
	}

	public void setMatches1(List<Match> matches1) {
		this.matches1 = matches1;
	}

	public Match addMatches1(Match matches1) {
		getMatches1().add(matches1);
		matches1.setTeam1(this);

		return matches1;
	}

	public Match removeMatches1(Match matches1) {
		getMatches1().remove(matches1);
		matches1.setTeam1(null);

		return matches1;
	}

	public List<Match> getMatches2() {
		return this.matches2;
	}

	public void setMatches2(List<Match> matches2) {
		this.matches2 = matches2;
	}

	public Match addMatches2(Match matches2) {
		getMatches2().add(matches2);
		matches2.setTeam2(this);

		return matches2;
	}

	public Match removeMatches2(Match matches2) {
		getMatches2().remove(matches2);
		matches2.setTeam2(null);

		return matches2;
	}

	public Manager getManager() {
		return this.manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Stadium getStadium() {
		return this.stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

	public List<Player> getAllPlayers() {
		List<Player> players = new ArrayList<Player>();
		for (int i = 0; i < contracts.size(); ++i)
			players.add(contracts.get(i).getPlayer());
		return players;
	}

	public List<Player> getPlayers() {
		List<Player> players = new ArrayList<Player>();

		for (int i = 0; i < contracts.size(); ++i)
			if (contracts.get(i).getPlayer().getTeamPosition() != null)
				players.add(contracts.get(i).getPlayer());

		return players;
	}
}