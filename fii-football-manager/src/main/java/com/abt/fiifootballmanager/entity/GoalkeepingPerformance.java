package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the GOALKEEPING_PERFORMANCES database table.
 * 
 */
@Entity
@Table(name="GOALKEEPING_PERFORMANCES")
@NamedQuery(name="GoalkeepingPerformance.findAll", query="SELECT g FROM GoalkeepingPerformance g")
public class GoalkeepingPerformance implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private GoalkeepingPerformancePK id;

	@Column(name="BAD_KICKS")
	private BigDecimal badKicks;

	@Column(name="BAD_PASSES")
	private BigDecimal badPasses;

	@Column(name="BAD_PUNCHES")
	private BigDecimal badPunches;

	@Column(name="GOOD_KICKS")
	private BigDecimal goodKicks;

	@Column(name="GOOD_PASSES")
	private BigDecimal goodPasses;

	@Column(name="GOOD_PUNCHES")
	private BigDecimal goodPunches;

	private BigDecimal mistakes;

	private BigDecimal rating;

	private BigDecimal saves;

	//bi-directional one-to-one association to Performance
	@OneToOne
	@JoinColumns({
		@JoinColumn(name="MATCH_ID1", referencedColumnName="MATCH_ID"),
		@JoinColumn(name="PLAYER_ID1", referencedColumnName="PLAYER_ID")
		})
	private Performance performance;

	public GoalkeepingPerformance() {
	}

	public GoalkeepingPerformancePK getId() {
		return this.id;
	}

	public void setId(GoalkeepingPerformancePK id) {
		this.id = id;
	}

	public BigDecimal getBadKicks() {
		return this.badKicks;
	}

	public void setBadKicks(BigDecimal badKicks) {
		this.badKicks = badKicks;
	}

	public BigDecimal getBadPasses() {
		return this.badPasses;
	}

	public void setBadPasses(BigDecimal badPasses) {
		this.badPasses = badPasses;
	}

	public BigDecimal getBadPunches() {
		return this.badPunches;
	}

	public void setBadPunches(BigDecimal badPunches) {
		this.badPunches = badPunches;
	}

	public BigDecimal getGoodKicks() {
		return this.goodKicks;
	}

	public void setGoodKicks(BigDecimal goodKicks) {
		this.goodKicks = goodKicks;
	}

	public BigDecimal getGoodPasses() {
		return this.goodPasses;
	}

	public void setGoodPasses(BigDecimal goodPasses) {
		this.goodPasses = goodPasses;
	}

	public BigDecimal getGoodPunches() {
		return this.goodPunches;
	}

	public void setGoodPunches(BigDecimal goodPunches) {
		this.goodPunches = goodPunches;
	}

	public BigDecimal getMistakes() {
		return this.mistakes;
	}

	public void setMistakes(BigDecimal mistakes) {
		this.mistakes = mistakes;
	}

	public BigDecimal getRating() {
		return this.rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public BigDecimal getSaves() {
		return this.saves;
	}

	public void setSaves(BigDecimal saves) {
		this.saves = saves;
	}

	public Performance getPerformance() {
		return this.performance;
	}

	public void setPerformance(Performance performance) {
		this.performance = performance;
	}

}