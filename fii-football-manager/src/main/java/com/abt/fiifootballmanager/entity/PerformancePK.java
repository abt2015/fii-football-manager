package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PERFORMANCES database table.
 * 
 */
@Embeddable
public class PerformancePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	
	@Column(name="MATCH_ID", insertable=false, updatable=false)
	private long matchId;
	
	@Column(name="PLAYER_ID", insertable=false, updatable=false)
	private long playerId;

	public PerformancePK() {
	}
	public long getMatchId() {
		return this.matchId;
	}
	public void setMatchId(long matchId) {
		this.matchId = matchId;
	}
	public long getPlayerId() {
		return this.playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PerformancePK)) {
			return false;
		}
		PerformancePK castOther = (PerformancePK)other;
		return 
			(this.matchId == castOther.matchId)
			&& (this.playerId == castOther.playerId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.matchId ^ (this.matchId >>> 32)));
		hash = hash * prime + ((int) (this.playerId ^ (this.playerId >>> 32)));
		
		return hash;
	}
}