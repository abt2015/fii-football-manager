package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="ACHIEVEMENTS")
@NamedQuery(name="Achievement.findAll", query="SELECT a FROM Achievement a")
public class Achievement implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AchievementPK id;

	//bi-directional many-to-one association to League
	@ManyToOne
	@JoinColumn(name="LEAGUE_ID", referencedColumnName="LEAGUE_ID",insertable = false, updatable = false)
	private League league;

	//bi-directional many-to-one association to Manager
	@ManyToOne
	@JoinColumn(name="MANAGER_ID",insertable = false, updatable = false)
	private Manager manager;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="TEAM_ID",insertable = false, updatable = false)
	private Team team;

	public Achievement() {
	}

	public AchievementPK getId() {
		return this.id;
	}

	public void setId(AchievementPK id) {
		this.id = id;
	}

	public League getLeague() {
		return this.league;
	}

	public void setLeague(League league) {
		this.league = league;
	}

	public Manager getManager() {
		return this.manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}