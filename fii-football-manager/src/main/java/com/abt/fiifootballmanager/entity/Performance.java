package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the PERFORMANCES database table.
 * 
 */
@Entity
@Table(name="PERFORMANCES")
@NamedQuery(name="Performance.findAll", query="SELECT p FROM Performance p")
public class Performance implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PerformancePK id;

	@Column(name="RED_CARD")
	private BigDecimal redCard;

	@Column(name="YELLOW_CARD")
	private BigDecimal yellowCard;

	//bi-directional one-to-one association to GoalkeepingPerformance
	@OneToOne(mappedBy="performance")
	private GoalkeepingPerformance goalkeepingPerformance;

	//bi-directional many-to-one association to OutfieldPerformance
	@OneToMany(mappedBy="performance")
	private List<OutfieldPerformance> outfieldPerformances;

	//bi-directional many-to-one association to Match
	@ManyToOne
	@JoinColumn(name="MATCH_ID",insertable = false, updatable = false)
	private Match match;

	//bi-directional many-to-one association to Player
	@ManyToOne
	@JoinColumn(name="PLAYER_ID",insertable=false, updatable=false)
	private Player player;

	public Performance() {
	}

	public PerformancePK getId() {
		return this.id;
	}

	public void setId(PerformancePK id) {
		this.id = id;
	}

	public BigDecimal getRedCard() {
		return this.redCard;
	}

	public void setRedCard(BigDecimal redCard) {
		this.redCard = redCard;
	}

	public BigDecimal getYellowCard() {
		return this.yellowCard;
	}

	public void setYellowCard(BigDecimal yellowCard) {
		this.yellowCard = yellowCard;
	}

	public GoalkeepingPerformance getGoalkeepingPerformance() {
		return this.goalkeepingPerformance;
	}

	public void setGoalkeepingPerformance(GoalkeepingPerformance goalkeepingPerformance) {
		this.goalkeepingPerformance = goalkeepingPerformance;
	}

	public List<OutfieldPerformance> getOutfieldPerformances() {
		return this.outfieldPerformances;
	}

	public void setOutfieldPerformances(List<OutfieldPerformance> outfieldPerformances) {
		this.outfieldPerformances = outfieldPerformances;
	}

	public OutfieldPerformance addOutfieldPerformance(OutfieldPerformance outfieldPerformance) {
		getOutfieldPerformances().add(outfieldPerformance);
		outfieldPerformance.setPerformance(this);

		return outfieldPerformance;
	}

	public OutfieldPerformance removeOutfieldPerformance(OutfieldPerformance outfieldPerformance) {
		getOutfieldPerformances().remove(outfieldPerformance);
		outfieldPerformance.setPerformance(null);

		return outfieldPerformance;
	}

	public Match getMatch() {
		return this.match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}