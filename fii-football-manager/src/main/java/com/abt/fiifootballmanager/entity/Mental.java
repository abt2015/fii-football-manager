package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@NamedQuery(name="Mental.findAll", query="SELECT m FROM Mental m")
public class Mental implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PLAYER_ID")
	private long playerId;

	private double aggression;

	private double anticipation;

	private double creativity;

	private double positioning;

	private double teamwork;

	private double workrate;

	//bi-directional one-to-one association to Skill
	@OneToOne
	@JoinColumn(name="PLAYER_ID")
	private Skill skill;

	public Mental() {
	}

	public long getPlayerId() {
		return this.playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public double getAggression() {
		return this.aggression;
	}

	public void setAggression(double aggression) {
		this.aggression = aggression;
	}

	public double getAnticipation() {
		return this.anticipation;
	}

	public void setAnticipation(double anticipation) {
		this.anticipation = anticipation;
	}

	public double getCreativity() {
		return this.creativity;
	}

	public void setCreativity(double creativity) {
		this.creativity = creativity;
	}

	public double getPositioning() {
		return this.positioning;
	}

	public void setPositioning(double positioning) {
		this.positioning = positioning;
	}

	public double getTeamwork() {
		return this.teamwork;
	}

	public void setTeamwork(double teamwork) {
		this.teamwork = teamwork;
	}

	public double getWorkrate() {
		return this.workrate;
	}

	public void setWorkrate(double workrate) {
		this.workrate = workrate;
	}

	public Skill getSkill() {
		return this.skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public double getSum() {
		return this.aggression + this.anticipation + this.creativity + this.positioning + this.teamwork + this.workrate;
	}

}