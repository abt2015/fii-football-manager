package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="\"PROFILE\"")
@NamedQuery(name="Profile.findAll", query="SELECT p FROM Profile p")
public class Profile implements Serializable {
	private static final long serialVersionUID = 1L;

	//@Id
	//@Column(name="MANAGER_ID")
	//private long managerId;
	
	@Id
	@Column(name="username")
	private String username;
	
	@Lob
	private byte[] avatar;

	@Column(name="FAVOURITE_TEAM")
	private String favouriteTeam;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="SECOND_NAME")
	private String secondName;

	//bi-directional one-to-one association to Manager
	@OneToOne
	@JoinColumn(name="MANAGER_ID")
	private Manager manager;

	public Profile() {
	}

	public void setUsername(String user){
		this.username = user;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	
	//public long getManagerId() {
	//	return this.managerId;
	//}

	//public void setManagerId(long managerId) {
	//	this.managerId = managerId;
	//}

	public byte[] getAvatar() {
		return this.avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public String getFavouriteTeam() {
		return this.favouriteTeam;
	}

	public void setFavouriteTeam(String favouriteTeam) {
		this.favouriteTeam = favouriteTeam;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return this.secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public Manager getManager() {
		return this.manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	//public String toString(){
	//	return //managerId + " " + firstName;
	//}
}