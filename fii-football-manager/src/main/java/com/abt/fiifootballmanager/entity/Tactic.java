package com.abt.fiifootballmanager.entity;

public enum Tactic {
	T1("4-4-2"), T2("4-3-3"), T3("4-4-1-1"), T4("4-2-3-1"), T5("4-1-3-2"), T6("3-4-3"), T7("3-5-2");
	
	private String tactic;
	
	private Tactic(String tactic){
		this.tactic = tactic;
	}
	
	public String getTactic(){
		return tactic;
	}
}
