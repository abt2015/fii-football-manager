package com.abt.fiifootballmanager.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GOALKEEPING_PERFORMANCES database table.
 * 
 */
@Embeddable
public class GoalkeepingPerformancePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="MATCH_ID1", insertable=false, updatable=false)
	private long matchId1;

	@Column(name="PLAYER_ID1", insertable=false, updatable=false)
	private long playerId1;

	public GoalkeepingPerformancePK() {
	}
	public long getMatchId1() {
		return this.matchId1;
	}
	public void setMatchId1(long matchId1) {
		this.matchId1 = matchId1;
	}
	public long getPlayerId1() {
		return this.playerId1;
	}
	public void setPlayerId1(long playerId1) {
		this.playerId1 = playerId1;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GoalkeepingPerformancePK)) {
			return false;
		}
		GoalkeepingPerformancePK castOther = (GoalkeepingPerformancePK)other;
		return 
			(this.matchId1 == castOther.matchId1)
			&& (this.playerId1 == castOther.playerId1);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.matchId1 ^ (this.matchId1 >>> 32)));
		hash = hash * prime + ((int) (this.playerId1 ^ (this.playerId1 >>> 32)));
		
		return hash;
	}
}