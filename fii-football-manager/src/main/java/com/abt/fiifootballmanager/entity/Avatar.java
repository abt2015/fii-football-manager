package com.abt.fiifootballmanager.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Avatar implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer avatarId;
	
	private byte[] image;
 
	public Avatar() {
	}
 
	public Avatar(byte[] image) {
		this.image = image;
	}
 
	public Integer getAvatarId() {
		return this.avatarId;
	}
 
	public void setAvatarId(Integer avatarId) {
		this.avatarId = avatarId;
	}
 
	public byte[] getImage() {
		return this.image;
	}
 
	public void setImage(byte[] image) {
		this.image = image;
	}
 
}
