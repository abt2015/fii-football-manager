package com.abt.fiifootballmanager.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.abt.fiifootballmanager.entity.Team;

import javax.persistence.ManyToOne;

import com.abt.fiifootballmanager.entity.Player;

@Entity
public class TransferOffer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long offerId;
	
	@ManyToOne
	private Team team;
	
	@ManyToOne
	private Player player;
	
	private int accepted;
	
	private double price;
	
	@Transient
	private long player_id;
	
	public long getOfferId() {
		return offerId;
	}
	
	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}
	
	public int getAccepted() {
		return accepted;
	}
	
	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}
	
	public Team getTeam() {
	    return team;
	}
	public void setTeam(Team param) {
	    this.team = param;
	}
	public Player getPlayer() {
	    return player;
	}
	public void setPlayer(Player param) {
	    this.player = param;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getPlayer_id() {
		return player_id;
	}

	public void setPlayer_id(long player_id) {
		this.player_id = player_id;
	}
	
	
}
