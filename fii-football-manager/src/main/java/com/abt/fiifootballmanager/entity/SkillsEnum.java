package com.abt.fiifootballmanager.entity;

public enum SkillsEnum {
	OUTFIELD, MENTAL, PHYSICAL, GOALKEEPING;
}
