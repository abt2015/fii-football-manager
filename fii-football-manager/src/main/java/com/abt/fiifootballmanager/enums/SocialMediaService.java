package com.abt.fiifootballmanager.enums;

public enum SocialMediaService {
	FACEBOOK, TWITTER
}
